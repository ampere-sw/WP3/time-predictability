#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import json
from pathlib import Path
from time_predictability.meta_parallel.app import App
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.time_analysis.analysis import TDGAnalysis


def analysis_example():
    """
    how to use the time analysis tool with the default metrics
    """
    input_tdg = 'examples/fixtures/tdgs/edge/edge.json'
    app:App = App.read_json(input_tdg)

    for tdg in app.tdgs: # or tdg:TDG = app.tdgs[0] to test only one tdg
        analysis = TDGAnalysis(4)
        # setup analysis options. Available metrics (in TDGAnalysis.metrics_names): volume, cpl, max_par, depth, makespan, wcrt, wcet, pmc
        # only makespan is not active by default, as it needs for all tasks to contain a static_thread
        analysis.exclude(['wcrt','pmc']) #list of metrics to exclude
        analysis.include(['volume']) #list of metrics to include (in this case it will not do anything as volume is already included)
        analysis.analyse_tdg(tdg)
        analysis.clear_results()

    out_json = analysis.app_to_json(app)
    Path("results").mkdir(parents=True, exist_ok=True)
    # e.g. how to write to file
    output_tdg = 'results/edge_analysis.json'
    with open(output_tdg, 'w') as outfile:
        json.dump(out_json, outfile,indent=2)

    #the following code is not necessary just to validate the results
    # tdg_expected_metrics = ['volume','depth','critical_path','max_parallel']
    # tdg_excluded_metrics = ['wcrt_tied', 'wcrt_untied']
    # task_expected_metrics = ['wcet']
    # task_excluded_metrics = ['42001000']#42001000 is a PMC which name [cycles] was not used but instead it was used its event ID
    # for task in out_json.values():
    #     if type(task) not in (int,float): #ignore metrics
    #         assert all(metric in task for metric in task_expected_metrics)
    #         assert all(metric not in task for metric in task_excluded_metrics)
    # assert all(metric in out_json for metric in tdg_expected_metrics)
    # assert all(metric not in out_json for metric in tdg_excluded_metrics)
    


analysis_example()