#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import json
from pathlib import Path
from time_predictability.meta_parallel.app import App
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.simulation.scheduler import MasterQueue
from time_predictability.simulation.simulator import Simulation, Simulator
from time_predictability.time_analysis.analysis import TDGAnalysis


def simulation_example():
    """
    how to use the simulator with an algorithm
    """
    input_tdg = 'examples/fixtures/tdgs/edge/edge.json'
    output_tdg = 'results/edge_static_mapping.json'
    task2thread = 'BestFit'
    queue = 'FIFO'
    queue_per_thread = True
    num_threads = 4
    app:App = App.read_json(input_tdg)
    for tdg in app.tdgs:
        tdg.avg_makespan()
        tdg.worst_makespan()
        tdg.volume()
        taskQueue = MasterQueue.get_algorithm_by_name(task2thread,queue,queue_per_thread,num_threads)
        simulator:Simulator = Simulator(tdg,taskQueue(),num_threads)
        simulation:Simulation = simulator.simulate()
        print("The selected algorithm provides a makespan of "+str(simulation.makespan))
        tdg.set_static_threads(simulation.get_mapping(),simulation.makespan)
    
    out_json = app.to_json(keep_results=False)
    # e.g. how to write to file
    Path("results").mkdir(parents=True, exist_ok=True)
    with open(output_tdg, 'w') as outfile:
        json.dump(out_json, outfile,indent=2)


simulation_example()