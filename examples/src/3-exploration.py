#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import json
from pathlib import Path
from time_predictability.mapping_exploration.explorer import ExplorationConfig, MappingExploration
from time_predictability.meta_parallel.app import App
from time_predictability.meta_parallel.tdg import TDG


def exploration_example():
    """
    how to use the mapping exploration tool
    """
    input = "examples/fixtures/tdgs/heat/heat_extrae_freq_2188.json"
    output = "results/heat_exploration_freq_2188.json"
    num_threads = 4
    target_tdgs =[1]
    deadlines = [280000000]
    map_algorithms = [
        {
            "task2thread": "BestFit",
            "queue": "BestFit",
            "queue_per_thread": True
        },
        {
            "task2thread": "BestFit",
            "queue": "FIFO",
            "queue_per_thread": True
        },
        {
            "task2thread": "BFS",
            "queue": "FIFO",
            "queue_per_thread": False
        },
        {
            "task2thread": "SEQR",
            "queue": "FIFO",
            "queue_per_thread": True
        }
    ]
    app:App = App.read_json(input)
    for tdg in app.tdgs:
        if tdg.id not in target_tdgs: #only apply to target tdgs
            continue 
        deadline = deadlines[target_tdgs.index(tdg.id)]
        exp_config:ExplorationConfig = ExplorationConfig(num_threads,deadline)
        for alg in map_algorithms:
            exp_config.add_algorithm_from_dict(alg)
            explorer:MappingExploration = MappingExploration(tdg,exp_config)
        success = explorer.explore()
        if success:
            alg_info = explorer.config.algorithms_info[explorer.best_position]
            print('The best mapping found is '+alg_info['task2thread']+'+'+alg_info['queue'] + ' with a makespan of '+str(explorer.best_makespan))
            explorer.apply_map()

        else:
            print('[Warning] The exploration was not able to find a static mapping providing a makespan lower than the given deadline.')
            print('[Warning] The output TDG was not generated.')
    out_json = tdg.to_json(keep_results=False)
    
    Path("results").mkdir(parents=True, exist_ok=True)
    with open(output, 'w') as outfile:
        json.dump(out_json, outfile,indent=2)

exploration_example()