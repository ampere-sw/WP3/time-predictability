#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.readlines()

MAJOR_VERSION = 0
MINOR_VERSION = 9
REVISION_VERSION = 31

setup(
    name='time-predictability',
    version=str(MAJOR_VERSION)+'.'+str(MINOR_VERSION)+'.'+str(REVISION_VERSION),
    packages=find_packages(),
    package_data={'': ['*.jar']},
    include_package_data=True,
    install_requires=requirements,
    license='Licensed under the Apache License, Version 2.0. Copyright 2022 Instituto Superior de Engenharia do Porto.',
    long_description=open('README.md').read(),
    url='https://gitlab.bsc.es/ampere-sw/WP3/time-predictability.git',
    entry_points={
        'console_scripts':[
            'time-analysis   = time_predictability.tools.cmd:time_analysis',
            'map-simulator   = time_predictability.tools.cmd:map_simulator',
            'map-exploration = time_predictability.tools.cmd:map_exploration',
            'dot2json        = time_predictability.tools.cmd:dots2json',
            'amxmi2json      = time_predictability.tools.cmd:amxmi2json'
        ]
    }
)
