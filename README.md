# time-predictability

This project is one of the contributions to WP3, in the context of providing time predictability to OpenMP applications. It is divided in three tools:

- Time Analysis
- Mapping Simulator
- Mapping Exploration

Each section describes what to do use each of the provided tools.

## Requirements
These are the requirements to run the time analysis API:
* Python 3.10

## Installation
The tool was developed with the intent to be used both as a module and as a command line tool. Please follow the following instructions depending on the type of use of the tool.

### As a python module
The easiest way of using this project as a module is to add it to the dependencies of your python project, which can be done by directly specifying the git repository as a dependency: 

```
$ pip install git+https://gitlab.bsc.es/ampere-sw/WP3/time-predictability.git
```

The project is automatically built, including the command line tools as explained in the following section. Therefore this approach works for both python module and for the command line tools.

### As a command line tool

The first step is to clone this project. Then, one can either create a virtual environment (to keep the project contents contained in a controlled environment) or simply jump this step to the next one and install the command line tools global to the system. 

#### Suggestion: use a Python Virtual Environment
To install this tool we suggest that a [python virtual environment](https://docs.python.org/3/library/venv.html) is used to keep the dependencies of the project localized. Inside the project folder run the following commands.

If in windows:
```PowerShell
> python3 -m venv <env_name>
> <env_name>\Scripts\activate
```

If in linux:
```sh
$ python3 -m venv <env_name>
$ source <env_name>/bin/activate
```

This will then put your command line with the format:
```sh
(env_name) \my\current\dir> 
or 
(env_name) /my/current/dir$ 
```

This means that all the Note that whenever you open a new terminal you will have to run again the command <code>source <name_for_the_env_folder/bin/activate</code> to again enter the environment variable. In the examples below a virtual environment named '.venv' is used. Therefore, every time an example of the command line appears with <code>(.venv) /a/path$</code> means that we are inside that environment.

#### Install the executables

Now we have to install the tools, including its dependencies. Since we are inside a virtual environment, the dependencies will only be installed in this environment, and not globally, thus not polluting the system.

Just run the following command and all dependencies are installed, and the executables are created and added to this environment.

```sh
$ python setup.py build
```

From this point forward one can use the tool as a command line tool or as an API.

## Usage
There are two ways of using the time-predictability project: as a module or as command line tools. Please see the one that fits best for your project.

### As a module
Assuming that this project is already in the dependencies of your project, at least two things are necessary: the meta-parallel module that contains classes for the TDG structure, and the module(s) of the tool(s) to be used. 

For the meta-parallel module, you need to import the classes to be used. The most important one, and potentially the only one you might
need to use, is the TDG class. This class expects a tdg.json and builds an instance of TDG, which will be used by all the other modules as an input:
```py
from time_predictability.meta_parallel.tdg import TDG
...
tdg = TDG.read_json(input_tdg)
```

Then you need to import the tool to be used and follow the instructions in the subsections below.

#### Time Analysis
The time analysis module is used to populate a TDG with metrics related to timing analysis of the parallel graph. 
The first step to use this module is to import it. E.g.:
```py
from time_predictability.time_analysis.analysis import TDGAnalysis
```

Then, one can use a class that calculates a set of metrics and automatically annotates the TDG. The simplest use of this class is as follow:

```py
analysis = TDGAnalysis(tdg,num_threads)
analysis.analyse()
output_json = analysis.tdg_to_json()
```
An instance has to be created with the TDG as input and a number of threads. This number of threads represent the number of threads available in the target system, in order calculate some of the metrics (e.g. the WCRT).
Then, the "analyse" function will calculate the metrics that are active by default. The last line regenerates the json that includes the calculated metrics. Note that during the execution of the "analyse" function is invoked, the tdg object is immediately annotated with the metrics, so it can be used in later steps.

<a name="metrics"></a>The following table contains a list of the available metrics, a brief description, and if they are active by default.
| key         | name                        | description                                                                                                                                                                                                 | level | active |
|-------------|-----------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------|:------:|
| wcet        | worst case execution time   | max execution time observed                                                                                                                                                                                 | task  |    x   |
| pmc         | performance counter metrics | metrics related to each pmc read                                                                                                                                                                            | task  |    x   |
| volume      | Volume                      | Total volume of the TDG                                                                                                                                                                                     | TDG   |    x   |
|  cpl        | critical path length        | Cost of the critical path                                                                                                                                                                                   | TDG   |    x   |
|  max_par    | maximum parallelism         | maximum possible level of parallelism                                                                                                                                                                       | TDG   |    x   |
| depth       | depth                       | maximum depth of the TDG                                                                                                                                                                                    | TDG   |    x   |
| makespan    | makespan                    | execution time from the source task to the sink task.  Requires all task to be annotated with ´static_thread´                                                                                               | TDG   |        |
| wcrt        | worst case response time    | Calculates an upper bound of the excepted worst execution time of the TDG. It presents the results for tied and untied tasks.  If the calculated metric is worst than the volume, then the volume is given. | TDG   |    X   |

To activate and deactivate metrics, one can create a list with a set of 'keys' (from the table) and use the following functions over the TDGAnalysis instance:

```py
to_include = ['makespan']
analysis.include(to_include)
to_exclude = ['wcrt','depth']
analysis.exclude(to_exclude)
```
Note that by including a list of metrics does not deactivate the others. If one desires, for instance, to only include one metric, let's say the 'wcet' of each task, then the function 'exclude_all' can be used for this end, as in the example below.

```py
analysis.exclude_all()
to_include = ['wcet']
analysis.include(to_include) #only wcet will be active
```

There is also a feature that allows one to remove the "results" property (that comes with the TDG) to clean up the output TDG:
```py
...
analysis.analyse()
analysis.clear_results()
output_json = analysis.tdg_to_json()
```
This example will generate the json containing the metrics, but without the results.

#### Mapping Simulator
This tool allows the simulation of OpenMP thread mapping algorithms over a target TDG, outputting the expected mapping of that algorithm, and the corresponding makespan.
It uses the timing analysis tool to extract the WCET of each task and simulates the execution of the TDG, where the task to thread mapping is responsibility of the selected algorithm.

The most important imports for this tool are the following:

```py
from time_predictability.simulation.scheduler import MasterQueue
from time_predictability.simulation.simulator import Simulation, Simulator
```
The first one is to build and instantiate the mapping algorithm, based on set of heuristics. The second ones are the simulator itself and the class that will contain the result of the simulation.

To build the algorithm, one can use the function 'get_algorithm_by_name', which requires the name of the heuristic for the task2thread mapping, the name of the heuristic for the queue, a boolean that says if this algorithm is to be used with a single queue (false) or a queue-per-thread approach, and the number of threads available for the simulation. The function returns a lambda that, when invoked, creates an instance of the algorithm.

<a name="heuristics"></a>For task2thread, currently the tool contains the following algorithms.

| key     | Description                                    | Queue per thread |
|---------|------------------------------------------------|------------------|
| BestFit | Selects the queue with least execution time    |        yes       |
| BFS     | Breadth-first schedule (OpenMP)                |        No        |
| SEQR    | Sequential Code, replicas in different threads |        yes       |

For the allocation queue, currently the tool contains the following algorithms.

| key     | Description                       |
|---------|-----------------------------------|
| BestFit | Selects the task with lower WCET  |
| FIFO    | First-in-first out queue          |

Then, we can use the Simulator class to create a simulation with an input TDG and the mapping, algorithm, and use the "simulate" method to execute the simulation. The following shows an example of how to use the simulator with the BestFit algorithm for task2thread, with a FIFO queue
```py
useMultiQueues = True
master = MasterQueue.get_algorithm_by_name('BestFit', 'FIFO', useMultiQueues, num_threads)
simulator:Simulator = Simulator(tdg, master(), num_threads)
simulation:Simulation = simulator.simulate()
```

After the simulation executes, a 'Simulation' class instance is returned, which contains the mapping computed during the simulation, organized both by task and by thread. In sequence, this mapping can be used over the TDG to statically define the threads for each task. The following is an example of using these two functionalities.

```py
static_map = simulation.get_mapping()
makespan = simulation.makespan
tdg.set_static_threads(static_map,makespan)
```

Each task in the TDG has now the static_thread property defined with the corresponding 'thread' in the static_map. We can now save the TDG with the given mapping in a file:

```py
output_tdg = 'output_tdg.json'
out_json = tdg.to_json(put_results=False)
with open(output_tdg, 'w') as outfile:
	json.dump(out_json, outfile, indent=2)
```

The 'tdg.to_json' method converts the TDG again to JSON format (a string), where the 'put_results' argument is simply used to remove the results that might exist inside the task.


#### Mapping Exploration
The mapping exploration API uses the two previous APIs as its basis, and builds an exploration environment to try and find the best task to thread mapping, based on a list of algorithms to make the mapping.
To start, two classes have to be imported:

```py
from time_predictability.mapping_exploration.explorer import ExplorationConfig, MappingExploration
```

The exploration requires two inputs: the target TDG and an exploration configuration. The configuration defines the number of available threads, the deadline, and the list of algorithms to use. The following is an example of creating the exploration with three algorithms and the execution of the exploration.


```py
tdg:TDG = TDG.read_json(input_file)
num_threads = 4
deadline = 2000000
exp_config:ExplorationConfig = ExplorationConfig(num_threads,deadline)
exp_config.add_algorithm('BestFit','BestFit',true)
exp_config.add_algorithm('BestFit','FIFO',true)
exp_config.add_algorithm('BFS','FIFO',false)

explorer:MappingExploration = MappingExploration(tdg,exp_config)
success = explorer.explore()
if success:
	explorer.apply_map()
	out_json = tdg.to_json(put_results=(not clear_results))
	with open(output_file, 'w') as outfile:
		json.dump(out_json, outfile,indent=2)
```

It is possible to see in the example that almost at the end we request the exploration with the 'explore' method, which returns a boolean. This boolean tells if the exploration was successful or not, i.e., if the exploration was able to find a mapping within the deadline. If so, 
by using the "apply_map" method it is possible to apply the best static mapping, obtained from the exploration, in the TDG, defining for each task its 'static_thread' property. The rest of the code is common procedure to convert the TDG (now annotated with a static mapping) into a JSON-format string and output it to a file.

### As a command line tool
#### Time Analysis
To execute as a command line, after installation, one can run the analysis-tool without argument to obtain the command options:
```sh
(.venv) my/work/place$ time-analysis
usage: [options] <input tdg> <output tdg>
     ---------------------------------------------------------------------------
    | option | argument           | description                                 |
     --------|--------------------|---------------------------------------------
    | -h     | n/a                | show this message                           |
    | -t     | <num_threads>      | specify max number of threads               |
    | -l     | n/a                | list metrics                                |
    | -x     | <metric(,metric)*> | set metrics that should not be calculated   |
    | -s     | <metric(,metric)*> | set metrics that should be calculated       |
    | -a     | <metric(,metric)*> | append metrics that should be calculated    |
    | -c     | n/a                | remove 'results' property from tasks        |
     ---------------------------------------------------------------------------
```
The tool requires an input tdg.json file, the output file location, and a set of options that define the number of threads (which defaults to 1) and the metrics to calculate. Many of the metrics are already active. To see the available metrics, and if they are active, the -l option can be used to print. To control which ones are active or inactive three options can be used: -x, -s and -a, with a list of keys (from the table) separated by a comma. The -x option deactivates the given list of metrics, -a activate the listed metrics (but does nothing with the others), and -s activates ONLY the ones specified in the given list.

The -c options makes that the output json, regardless of the calculated metrics, does not include the results in each task. It serves as a way to have a clean TDG, where only the metrics are listed.

#### Mapping Simulator
To execute as a command line, after installation, one can run the map-exploration without argument to obtain the command options:
```sh
(.venv) my/work/place$ map-simulator
usage: [options] <input tdg> <output tdg>
     ----------------------------------------------------------------------------------
    | option | argument           | description                                        |
     --------|--------------------|----------------------------------------------------
    | -h     | n/a                | show this message                                  |
    | -l     | n/a                | list heuristics                                    |
    | -n     | <num_threads>      | specify number of threads available (def: 1)       |
    | -t     | <task2thread>      | specifies the scheduling algorithm (def: BFS)      |
    | -q     | <allocation_queue> | list metrics that should be calculated (def: FIFO) |
    | -m     | n/a                | use one queue per thread (instead of single queue) |
    | -c     | n/a                | remove 'results' property from tasks               |
     ----------------------------------------------------------------------------------
```
The tool requires an input tdg.json file, the output file location, and a set of options that define the number of threads (which defaults to 1) and the heuristics to use for each phase of the mapping algorithm. To see the available heuristics one can use the -l option to print them. Use -t to specify the heuristic for the task to thread mapping phase and -q to specify the heuristic for the queue. The -m option is used to specify that the algorithm should work with multiple queues, one per queue (if not used then just one queue is used for all the system). Take into account that some of the algorithms do not work with single/multiple algorithms. Please see the table [above](#heuristics) to see if the algorithm uses multiple queues or not.

The -c options makes that the output json, regardless of the calculated metrics, does not include the results in each task. It serves as a way to have a clean TDG, where only the metrics are listed.


#### Mapping Exploration
To execute as a command line, after installation, one can run the map-exploration tool without argument to obtain the command options:
```sh
(.venv) my/work/place$ map-exploration
usage: [options] <config.json>
     -=================================== Options ===========================-
    | option | argument  | description                                        |
     --------|-----------|----------------------------------------------------
    | -h     | n/a       | show this message                                  |
    | -l     | n/a       | list heuristics                                    |
    | -c     | n/a       | remove 'results' property from tasks               |
     -=======================================================================-

     -====================== Configuration File Properties ===================================-
    | property            | argument      | description                                        |
     ---------------------|---------------|----------------------------------------------------
    | input               | <tdg.json>    | input tdg json file                                |
    | output              | <tdg.json>    | output tdg json file                               |
    | num_threads         | int           | number of threads for the simulator                |
    | deadline            | int           | maximum makespan the TDG can have                  |
    | map_algorithms      | [algorithms+] | list of mapping algorithm specifications           |
     -========================================================================================-
     -=========================== For each algorithm =========================================-
    | property            | argument      | description                                        |
     ---------------------|---------------|----------------------------------------------------
    | task2thread         | <heuristic>   | one of the available heuristics for task2thread    |
    | queue               | <heuristic>   | one of the heuristics for allocation queue         |
    | queue_per_thread    | true|false    | specifies if the algorithm has one queue per thread|
     -========================================================================================-
```

The tool requires as input a configuration file which contains the parameters necessary for the exploration, more specifically: the input and output TDG files, number of threads, the deadline, and a list of algorithms to use for the exploration. Regarding the algorithms, they can be a combination of the heuristics defined in [this table](#heuristics). The -l option can also be used to see these heuristics. A configuration file looks something like the following example:

```py
{
    "input": "examples/fixtures/tdgs/heat/heat_extrae_freq_2188.json",
    "output": "results/heat_extrae_freq_2188.json",
    "num_threads": 4,
    "deadline": 280000000,
    "map_algorithms":[
        {
            "task2thread": "BestFit",
            "queue": "BestFit",
            "queue_per_thread": true
        },
        {
            "task2thread": "BestFit",
            "queue": "FIFO",
            "queue_per_thread": true
        },
        {
            "task2thread": "BFS",
            "queue": "FIFO",
            "queue_per_thread": false
        },
        {
            "task2thread": "SEQR",
            "queue": "FIFO",
            "queue_per_thread": true
        }
    ]
}
```
The tool is then going to process the input TDG, experiment with the different algorithms and, at the end, output a TDG file with the best static mapping found. If no static mapping provides a makespan less than the given deadline, then the output TDG is not generated, as no fitting mapping was found with the provided parameters. The following is an example of running the tool with the previous configuration file:

```sh
(.venv) my/work/place$ map-exploration -c examples/fixtures/exp_configs/heat/heat_freq_2188.json
	algorithm BestFit(BestFit) is within deadline:  278122897 <= 280000000
	algorithm BestFit(FIFO) is within deadline:  276962046 <= 280000000
	algorithm BFS(FIFO) is within deadline:  271898301 <= 280000000
	algorithm SEQR(FIFO) is not within deadline:  992947740 > 280000000
The best mapping found is BFS+FIFO with a makespan of 271898301
```

## Examples
To see how the tools can be used and see them running, three examples are provided inside the "examples/src" folder.

## 1-analysis.py
Open this source file to see how to use the time analysis tool. To execute it simply run the following command:
```sh
(.venv) my/work/place$ python ./examples/src/1-analysis.py
```

## 2-simulation.py
Open this source file to see how to use the simulation tool. To execute it simply run the following command:
```sh
(.venv) my/work/place$ python ./examples/src/2-simulation.py
```

## 3-exploration.py
Open this source file to see how to use the mapping exploration tool. To execute it simply run the following command:
```sh
(.venv) my/work/place$ python ./examples/src/3-exploration.py
```

## Authors and acknowledgment
ISEP Contributors:
  - Tiago Carvalho
  - Luís Miguel Pinho
  - Mohammad Gharajeh

## License
Copyright 2022 Instituto Superior de Engenharia do Porto. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

## Project status
Under Development in the context of the [AMPERE Project](https://ampere-euproject.eu/).


## Related Repositories
Multi-criteria configuration: https://gitlab.bsc.es/ampere-sw/wp2/multicriteria-optimization/multicriteria-configuration
