#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from math import inf
from typing import Dict, List
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.simulation.scheduler import MasterQueue
from time_predictability.simulation.simulator import Simulation, Simulator

from time_predictability.simulation.thread import TaskQueue

class ExplorationConfig():
    def __init__(self,num_threads=1, deadline=inf):
        self.algorithms: List[MasterQueue] = []
        self.algorithms_info: List[Dict] = []
        self.num_threads = num_threads
        self.deadline = deadline if deadline > 0 else inf
    
    def add_algorithm(self, task2thread:str, queue:str, queue_per_thread:bool):
        algorithm_info = {'task2thread':task2thread,'queue':queue,'queue_per_thread':queue_per_thread}
        alg = MasterQueue.get_algorithm_by_name(task2thread,queue,queue_per_thread,self.num_threads)
        self.algorithms.append(alg)
        self.algorithms_info.append(algorithm_info)
    
    def add_algorithm_from_dict(self, json:Dict):
        algorithm_info = {'task2thread':json['task2thread'],'queue':json['queue'],'queue_per_thread':json['queue_per_thread']}
        alg = MasterQueue.get_algorithm_by_name(json['task2thread'],json['queue'],json['queue_per_thread'],self.num_threads)
        self.algorithms.append(alg)
        self.algorithms_info.append(algorithm_info)

    # def from_json(json:Dict) -> 'ExplorationConfig':
    #     deadline = int(json['deadline'])
    #     num_threads = int(json['num_threads'])
    #     exp = ExplorationConfig(num_threads,deadline)

        


class MappingExploration():
    def __init__(self, tdg:TDG, config: ExplorationConfig) -> None:
        self.tdg:TDG = tdg
        self.config:ExplorationConfig = config
        self.maps = [None]*len(config.algorithms)
        self.makespans = [None]*len(config.algorithms)
        self.best_position = -1
        self.best_makespan = inf
        self._explored = False
    
    def explore(self) -> bool:
        """
        Simulates all the given algorithms with the provided TDG.
        Returns true if a mapping with makespan <= deadline was found, false otherwise
        """
        #calculate wcets
        self.tdg.calc_tasks_wcet()
        #then, simulate execution with the mapping algorithms 
        #minimum makespan will be always <= deadline
        min_simulation:Simulation = None
        algorithm:MasterQueue
        print("[EXPLORER]Exploring different mappings for tdg",self.tdg.id)
        for a,algorithm in enumerate(self.config.algorithms):
            alg_info = self.config.algorithms_info[a]
            alg_type = alg_info['task2thread']
            queue_type =  alg_info['queue']
            #get algorithm instantiator by name
            alg_name = alg_type+"("+queue_type+")"
            #simulate with the given algorithm, with the setted number of threads
            simulator: Simulator = Simulator(self.tdg,algorithm(),self.config.num_threads)
            simulation: Simulation = simulator.simulate()
            #set makespan in algorithm
            self.makespans[a] = simulation.makespan
            self.maps[a] = simulation.get_mapping()
            #if makespan is withing deadline compare it to the current one with the least makespan
            if simulation.makespan <= self.config.deadline:
                if self.config.deadline == inf:
                    print("[EXPLORER] Algorithm",alg_name,"has makespan of",simulation.makespan)
                else:
                    print("[EXPLORER] Algorithm",alg_name,"is within deadline: ",simulation.makespan,"<=",self.config.deadline)
                if not min_simulation or simulation.makespan < min_simulation.makespan:
                    min_simulation = simulation
                    self.best_makespan = simulation.makespan
                    self.best_position = a
            else:
                print("[EXPLORER] Algorithm",alg_name,"is not within deadline: ",simulation.makespan,">",self.config.deadline)
        self._explored = True
        #this means that the tool did not find any algorithm providing makespan less than deadline
        #which in turn means that this algorithm is not suitable for the RT criteria
        if self.best_position < 0:
            print("[EXPLORER][WARNING] the tool was not able to provide a mapping for TDG",self.tdg.id,"with makespan within deadline")
            return False
        
        return True

    def apply_map(self):
        if not self._explored:
            print("[EXPLORER][WARNING] before applying a map the exploration must be performed via MappingExploration.explore method.")
        elif self.best_position == -1:
            print("[EXPLORER][WARNING] map could not be applied as no viable mapping was found for TDG",self.tdg.id,".with makespan within deadline.")
        else:
            self.tdg.set_static_threads(self.maps[self.best_position],self.best_makespan)
            