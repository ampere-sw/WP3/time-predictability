#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from functools import reduce
from typing import Callable, List
from time_predictability.meta_parallel.task import Task
from time_predictability.simulation.thread import Thread

class TaskQueue:
    """
    Represents a Queue used to organize tasks to be executed. For an example see DefaultTaskQueue.
    """
    def push(self, list:List[Task]):
        pass

    def pop(self) -> Task:
        pass

    def len(self) -> int:
        pass
    
    def time_volume(self) -> int:
        pass

    def clear(self):
        pass

    def is_empty(self):
        pass

    def threads(self):
        pass

    def setup_thread(self, thread):
        """
        Should return a reference to the queue corresponding to this thread.
        For instance, if a "master queue" deals with multiple queues, it should point out to the correct queue
        """
        pass

    def list_tasks(self):
        pass


def sort_list(list:List[Task],reverse=False) -> None:
    return list.sort(key=lambda t: t.metrics.wcet(),reverse=reverse)
def descent_order(list:List[Task]) -> List[Task]:
    return sort_list(list,True)
def ascent_order(list:List[Task]) -> List[Task]:
    return sort_list(list,False)
def default_order(list:List[Task]) -> List[Task]:
    return list


class DefaultTaskQueue(TaskQueue):
    """
    Class with default operations for a (Task)Queue, using a sorter, on push, to sort  new tasks
    The pop action will remove the first element of the list (index = 0)
    """
    def __init__(self, queue_sorter: Callable[[List[Task]],None]=None):
        self.queue: List[Task] = []
        self.queue_sorter = queue_sorter if queue_sorter else default_order
        self.threads = []

    def set_queue_sorter(self, sorter:Callable[[List[Task]],None]):
        self.queue_sorter = sorter
    def push(self, list:List[Task]):
        self.queue.extend(list)
        self.queue_sorter(self.queue)

    def time_volume(self) -> int:
        return reduce(lambda l,r: l+r.metrics.wcet(),self.queue,0)

    def threads(self):
        return self.threads    
    def setup_thread(self, thread):
        self.threads.insert(thread.id,thread)
        return self

    def pop(self) -> Task:
        return self.queue.pop(0)
    def len(self) -> int:
        return len(self.queue)
    def clear(self):
        self.queue.clear()
    def is_empty(self):
        return self.len() == 0

    def list_tasks(self):
        return [t for t in self.queue]

    def __str__(self) -> str:
        return "queue: "+ str([task.id+"("+str(task.metrics.wcet())+")" for task in self.queue])


class AllocationQueue(DefaultTaskQueue):
    """
    Represent a queue associated to a thread
    """
    def __init__(self,sorter: Callable[[List[Task]],None]):
        super().__init__(sorter)


# Predefined Algorithms for the allocation phase

class FIFO_AllocationQueue(AllocationQueue):
    """
    A queue that does not organize the tasks. I.e., a first-in-first-out queue.
    """
    def __init__(self):
        super().__init__(None)


class BestFit_AllocationQueue(AllocationQueue):
    """
    The best fit allocation queue: select task with least execution time
    """
    def __init__(self):
        super().__init__(ascent_order)