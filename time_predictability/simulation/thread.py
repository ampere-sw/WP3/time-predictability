#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#Interface used to provide a Queue
from typing import List
from time_predictability.meta_parallel.task import Task

class TaskQueue:
    def push(self, list:List[Task]):
        pass

    def pop(self) -> Task:
        pass

    def len(self) -> int:
        pass
    
    def time_volume(self) -> int:
        pass

    def clear(self):
        pass

    def is_empty(self):
        pass

    def threads(self):
        pass

    def setup_thread(self, thread):
        """
        Should return a reference to the queue corresponding to this thread.
        For instance, if a "master queue" deals with multiple queues, it should point out to the correct queue
        """
        pass

    def list_tasks(self):
        pass

class Thread:
    def __init__(self, id, queue: TaskQueue):
        self.id = id
        self.task = None
        self.task_start_time = 0
        self.remaining_time = 0
        self.elapsed_time = 0
        self.queue = queue.setup_thread(self)
        self.current_idle_time = 0

    def __str__(self):
        return "Thread"+self.to_json()

    def to_json(self):
        return "{id: "+str(self.id) \
            + ", task: "+ (str(self.task.id) if self.task else "None") \
            + ", remaining_time: "+str(self.remaining_time) \
            + ", elapsed_time: "+str(self.elapsed_time) \
            + ", idle_time: "+str(self.current_idle_time)+"}"
    
    def __repr__(self):
        return self.__str__()