#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#Interface used to provide a Queue
from functools import reduce
from typing import Callable, List
from time_predictability.meta_parallel.task import Task
from time_predictability.simulation.thread import Thread
from time_predictability.simulation.queue import FIFO_AllocationQueue, TaskQueue, DefaultTaskQueue, BestFit_AllocationQueue


class TaskScheduler:
    """
    Class used by QueuePerThread class to select the queue to which a given task should be scheduled
    """
    def allocate(self, masterQueue:"QueuePerThread_Master", task:Task) -> int:
        """
        From the threads existing in the masterQueue, select the best one for the input task
        """
        pass

class BestFit_Scheduler(TaskScheduler):
    def allocate(self, masterQueue:"QueuePerThread_Master", task:Task):
        return self.allocate_mtet_mrit(masterQueue, task)

    def comparator(q: TaskQueue):
        """
        order by least volume in queue, then most idle time (i.e. most time without work)
        """
        thread: Thread = q.threads[0]
        return (q.time_volume(), -thread.current_idle_time)
    
    def allocate_mtet_mrit(self, queue_per_task:"QueuePerThread_Master", task:Task):
        sorted_queue = sorted(queue_per_task.queues, key=BestFit_Scheduler.comparator)
        selected_queue: TaskQueue
        if task.has_replicas():
            queue: TaskQueue
            for queue in sorted_queue:
                replicas = [rep for rep in queue.list_tasks() if rep in task.replicas]
                if not replicas:
                    selected_queue = queue
                    break
            if not selected_queue:
                selected_queue = sorted_queue[0]
        else: 
            selected_queue = sorted_queue[0]
        selected_queue.push([task])
        i = queue_per_task.queues.index(selected_queue)
        queue_per_task.acc_queues[i] += task.metrics.wcet()
        return i

    def allocate_by_acc(self, masterQueue:"QueuePerThread_Master", task:Task):
        queue = min(masterQueue.acc_queues)
        i = masterQueue.acc_queues.index(queue)
        masterQueue.queues[i].push([task])
        masterQueue.acc_queues[i] += task.metrics.wcet()
        return i

class StaticThread_Scheduler(TaskScheduler):
    def allocate(self, masterQueue:"QueuePerThread_Master", task:Task):
        masterQueue.queues[task.static_thread].push([task])
        return task.static_thread


class Separate_replicas_scheduler(TaskScheduler):
    def allocate(self, queue_per_task:"QueuePerThread_Master", task:Task):
        sorted_queue = queue_per_task.queues
        selected_queue: TaskQueue
        if task.has_replicas():
            queue: TaskQueue
            for i, queue in enumerate(sorted_queue):
                replicas = [rep for rep in queue.list_tasks() if rep in task.replicas]
                if not replicas:
                    selected_queue = queue
                    break
            if not selected_queue:
                selected_queue = sorted_queue[0]
        else: 
            selected_queue = sorted_queue[0]
        selected_queue.push([task])
        i = queue_per_task.queues.index(selected_queue)
        queue_per_task.acc_queues[i] += task.metrics.wcet()
        return i


class MasterQueue(TaskQueue):
    def __init__(self):
        pass

    def get_algorithm_by_name(task2thread: str, queue: str, queue_per_thread: bool, num_queues:int) -> 'MasterQueue':
        """
        This function converts the "task2thread" and "queue" name into the corresponding classes. 
        Note that if you define both task2thread and queue, this will automatically put <num_queues> queues inside the master.
        If only queue is setted, then only one queue is created and returned
        """
        master_c = MasterQueue.__get_scheduler_by_name(task2thread)
        if not master_c:
            raise Exception("The type of mapping algorithm given does not exist:",task2thread)
        
        queue_c = MasterQueue.__get_allocation_queue_class(queue)
        if not queue_c:
            raise Exception("The type of queue given does not exist:",task2thread)
        return lambda: MasterQueue.__instantiate_alg(master_c, queue_c, queue_per_thread, num_queues)

    def __get_allocation_queue_class(queue: str) -> TaskQueue:
        return allocation_queue_algorithms[queue] if queue in allocation_queue_algorithms else None

    def __get_scheduler_by_name(algorithm: str) -> 'MasterQueue':
        return task2Thread_algorithms[algorithm] if algorithm in task2Thread_algorithms else None

    def __instantiate_alg(algorithm_c, queue_c, queue_per_thread: bool, num_queues:int):
        
        queues = queue_c() if not queue_per_thread else  [queue_c() for t in range(num_queues)]
        return algorithm_c(queues)


class SingleQueue_Master(MasterQueue):
    """
    Mapping algorithm considering a single queue
    """
    def __init__(self, allocation_queue: TaskQueue):
        self.queue: TaskQueue = allocation_queue

    def push(self, tasks:List[Task]):
        return self.queue.push(tasks)
            
    def setup_thread(self, thread):
        return self.queue.setup_thread(thread)

    def pop(self) -> Task:
        return self.queue.pop()
    def len(self) -> int:
        return self.queue.len()
    def clear(self):
        self.queue.clear()
    def is_empty(self):
        return self.len() == 0

    def __str__(self) -> str:
        return str(self.queue)

class QueuePerThread_Master(MasterQueue):
    """
    Build a structure of a queue manager that manages a queue per thread, deciding to which queue a task is mapped when ready
    """
    def __init__(self, allocation_queues: List[TaskQueue], task_scheduler: TaskScheduler):
        self.queues: List[TaskQueue] = allocation_queues
        self.queue_selector:TaskScheduler = task_scheduler
        self.acc_queues:List[int] = [0 for t in range(len(allocation_queues))]

    def push(self, tasks:List[Task]):
        for task in tasks:
            i = self.queue_selector.allocate(self, task)
            
    def setup_thread(self, thread):
        return self.queues[thread.id].setup_thread(thread)

    def pop(self) -> Task:
        raise Exception('Master Queue should not be asked to pop!')
    def len(self) -> int:
        return reduce(lambda acc,q: acc+q.len(),self.queues,0)
    def clear(self):
        for queue in self.queues:
            queue.clear()
    def is_empty(self):
        return self.len() == 0

    def __str__(self) -> str:
        ret = ""
        for i, queue in enumerate(self.queues):
            ret+="queue "+str(i)+": (acc="+str(self.acc_queues[i])+") "+str(queue)+"\n"
        return ret


# The following should be the algorithms to be used, already considering the combinations

# Predefined Algorithms for the scheduling phase

class BestFit_Master(QueuePerThread_Master):
    """
    The best fit scheduler: select thread with least volume and most idle time
    """
    def __init__(self, allocation_queues: List[TaskQueue]):
        super().__init__(allocation_queues, BestFit_Scheduler())

class StaticThread_Master(QueuePerThread_Master):
    def __init__(self, num_threads:int):
        super().__init__([DefaultTaskQueue() for t in range(num_threads)], StaticThread_Scheduler())

class SequentialWithSeparateReplicas_Scheduler(QueuePerThread_Master):
    def __init__(self,  allocation_queues: List[TaskQueue]):
        super().__init__(allocation_queues, Separate_replicas_scheduler())

class BFS_GOMP(SingleQueue_Master):
    def __init__(self,  allocation_queue: TaskQueue):
        super().__init__(allocation_queue)

# Predefined Algorithms with both phases defined

class BestFit_w_BestFit(BestFit_Master):
    """
    Best fit scheduler with best fit allocation queue
    The best fit scheduler: select thread with least volume and most idle time
    The best fit allocation queue: select task with least execution time
    """
    def __init__(self,  num_threads:int):
        super().__init__([BestFit_AllocationQueue() for t in range(num_threads)])

class BFS_GOMP_FIFO(BFS_GOMP):
    """
    Single threaded BFS with a FIFO
    """
    def __init__(self):
        super().__init__(FIFO_AllocationQueue())


allocation_queue_algorithms = {
    'FIFO': FIFO_AllocationQueue,
    'BestFit': BestFit_AllocationQueue
}

task2Thread_algorithms = {
    'BestFit': BestFit_Master,
    'BFS': BFS_GOMP,
    'SEQR': SequentialWithSeparateReplicas_Scheduler
}




