#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from time_predictability.meta_parallel.task import Task
from time_predictability.simulation.queue import BestFit_AllocationQueue
from time_predictability.simulation.thread import TaskQueue, Thread
from time_predictability.time_analysis.analysis import TDG
from typing import Dict, List
from functools import reduce
 
        
class TaskExecutionSimulation:
    def __init__(self,id:int,thread:int, init_time:int):
        self.id = id
        self.thread = thread
        self.init_time = init_time
        self.fini_time = -1
        self.exec_time = -1

    def end(self, fini_time:int):
        self.fini_time = fini_time
        self.exec_time = fini_time - self.init_time

class ThreadWorkSimulation:
    def __init__(self,id):
        self.id = id
        self.tasks:List[TaskExecutionSimulation] = []

    def add_task(self, taskExecSimul: TaskExecutionSimulation):
        self.tasks.append(taskExecSimul)
    
    def busy_time(self):
        return reduce(lambda acc, t: acc+t.exec_time,self.tasks,0)

    def ordered_tasks(self):
        self.tasks.sort(key=lambda t: t.init_time,reverse=False)
        return self.tasks

class WaitingTask:
    def __init__(self, task:Task):
      self.task = task
      self.ins = 0
      self.target_ins = len(task.ins)
      self.edges = {}

    def __str__(self):
        return "{task:"+str(self.task.id) \
            +", ins:"+str(self.ins) \
            +", target_inst:"+str(self.target_ins)+"}"

    def __repr__(self):
        return self.__str__()

class Execution:
    def __init__(self,state,task_id,thread_id,init_time,fini_time):
      self.state = state
      self.task_id = task_id 
      self.thread_id = thread_id 
      self.init_time = init_time 
      self.fini_time = fini_time 

    def __str__(self):
        return "{state:"+self.state\
            +", task:"+str(self.task_id) \
            +", thread:"+str(self.thread_id) \
            +", start:"+str(self.init_time) \
            +", finish:"+str(self.fini_time)+"}"

    def __repr__(self):
        return self.__str__()

class Simulation:
    def __init__(self,num_threads:int):
        self.makespan = -1
        self.execution: List[Execution] = []
        self.tasks: Dict[str,TaskExecutionSimulation] = {}
        self.threads: List[ThreadWorkSimulation] = [ThreadWorkSimulation(id) for id in range(num_threads)]
    
    def get_mapping(self) -> Dict[str,int]:
        return {task: self.tasks[task].thread for task in self.tasks}

class Simulator:
    def __init__(self,tdg: TDG, taskQueue: TaskQueue = None, num_threads: int = 1):
        self.tdg = tdg
        self.queue = taskQueue if taskQueue != None else BestFit_AllocationQueue()
        self.waitingTasks: List[WaitingTask] = []
        self.simulation: Simulator=None 
        self.num_threads = num_threads
        self.threads: List[Thread] = [Thread(k,self.queue) for k in range(self.num_threads)]

    def set_task_queue(self, taskQueue: TaskQueue):
        self.queue = taskQueue

    #add another task to the waiting list. 
    # the waiting list are the tasks that recieved the input from a parent task and are waiting for the other input tasks.
    # if the task is already in the list, then just update the total "ready ins" for that task
    def addWaitingTasks(self, tasks: List[Task]):
        for task in tasks:
            waiting = next((waiting for waiting in self.waitingTasks if waiting.task == task), None)
            if waiting == None:
                waiting = WaitingTask(task)
                self.waitingTasks.append(waiting)
            waiting.ins += 1            

    #a task moves from the waiting list to the ready list when it has all necessary ins (i.e. len(task.ins) == waiting.ins)
    def moveWaitingToReady(self):
        newReady:List[Task] = []
        for waiting in self.waitingTasks:
            if waiting.ins >= waiting.target_ins:
                newReady.append(waiting.task)
                # waitingTasks.remove(waiting)
        for task in list(self.waitingTasks):
            if task.task in newReady:
                self.waitingTasks.remove(task)
        # self.waitingTasks = [task for task in self.waitingTasks if task.task not in newReady]    
        # newReady.sort(key=lambda t: t.metrics.exec_time.max(),reverse=True)
        self.queue.push(newReady)
        # readyTasks.extend(newReady)

    
    #default num threads is 1 (i.e., single threaded)
    def simulate(self) -> Simulation:
        self.simulation = Simulation(self.num_threads)
        history: List[Execution] = []
        

        self.waitingTasks.clear()
        self.queue.clear()

        roots = self.tdg.sources()# [root for root in self.tdg.tasks if not root.ins]
        self.addWaitingTasks(roots)

        count = 0
        self.total_time_elapsed = 0
        while self.waitingTasks \
            or not self.queue.is_empty() \
            or len([thread for thread in self.threads if thread.task is not None]):
            
            self.moveWaitingToReady()
            # print("After moving tasks to queue: \n", self)
            # input("key...")
            for thread in (thread for thread in self.threads if thread.task is None):
                if thread.queue.is_empty():
                    continue # no ready tasks waiting to work
                task: Task = thread.queue.pop()
                thread.task = task
                thread.task_start_time = self.total_time_elapsed
                # task.metrics.avg_time() # for comparing purposes
                thread.remaining_time = task.metrics.wcet()
                thread.current_idle_time = -task.metrics.wcet()
                thread.elapsed_time = 0
                history.append(Execution("STARTED",task.id, thread.id, thread.task_start_time, thread.task_start_time+thread.remaining_time))
                self.simulation.tasks[task.id] = TaskExecutionSimulation(task.id,thread.id,thread.task_start_time)
                self.simulation.threads[thread.id].add_task(self.simulation.tasks[task.id])
                # print("After moving task",task.id,"to thread",thread.id,"\n",self)
                # input("key...")
            workingThreads = [thread for thread in self.threads if thread.task is not None]
            idleThreads = [thread for thread in self.threads if thread.task is None]
            current_elapsed_time = min(workingThreads, \
                    key=lambda thread:thread.remaining_time).remaining_time
            self.total_time_elapsed += current_elapsed_time
            #could be done for all threads, but it is here if more code for idle threads is needed
            for thread in idleThreads:
                thread.current_idle_time += current_elapsed_time

            for thread in workingThreads:
                thread.current_idle_time += current_elapsed_time
                thread.elapsed_time += current_elapsed_time
                thread.remaining_time -= current_elapsed_time
                if thread.remaining_time == 0:
                    task = thread.task
                    thread.task = None
                    history.append(Execution("FINISH",task.id, thread.id, thread.task_start_time, thread.task_start_time+thread.elapsed_time))
                    self.simulation.tasks[task.id].end(thread.task_start_time+thread.elapsed_time)
                    self.addWaitingTasks(task.outs)
            # print("#"+str(count)+str(history))
            count+=1
        
        self.simulation.makespan = self.total_time_elapsed
        self.simulation.execution = history

        return self.simulation

    def __str__(self) -> str:
        ret = ''
        ret += 'time: '+str(self.total_time_elapsed)
        ret += '\nwaiting tasks: '+str([wtask.task.id for wtask in self.waitingTasks])+"\n"
        ret += str(self.queue)
        for thread in self.threads:
            ret += "\n"+str(thread)
        return ret

