#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import math
from time_predictability.meta_parallel.task import Task
from time_predictability.meta_parallel.tdg import TDG


def traditional_wcrt(vol,len,factor,m):
    return len + factor/m * (vol-len)

def untied_wcrt(vol, len, max_par,m):
    real_m = m if m <= max_par else max_par
    return traditional_wcrt(vol,len,1,real_m)

def tied_wcrt_form_1(vol, len, max_par, depth, m):
    real_m = m if m <= max_par else max_par
    return traditional_wcrt(vol,len,1+depth,real_m)

def tied_wcrt_form_2(tdg:TDG, vol, max_par, m):
    """
    Deprecated as it gave very pessimistic values
    """
    timing_data = tdg.latest_times()
    real_m = m if m <= max_par else max_par
    len_v_g = len_v(tdg,timing_data,real_m)
    sum_w_tied = sum(timing_data[k].latest_start for k in timing_data)
    rub = (vol + len_v_g + sum_w_tied)/real_m
    return rub

def delta_v(task:Task, current_time, c_v_ix, data):
    data[task.id]['max'] = max(data[task.id]['max'], current_time+c_v_ix[task.id])
    data[task.id]['ins']+=1
    if data[task.id]['ins'] >= len(task.ins):
        for cTask in task.outs:
            delta_v(cTask, data[task.id]['max'],c_v_ix,data)

def len_v(tdg:TDG, timing_data, m):
    # this is for the formula: (m-1)c_ix - len(lamb_ix)
    # c_ix = {k: (m-1)*data.wcet - data.latest_start for (k,data) in timing_data.items() }
    c_v_ix = {k: (m-1)*data.wcet - data.latest_start for (k,data) in timing_data.items() }
    data = {k: {'max':-math.inf,'ins':0} for k in timing_data}
    delta_v(tdg.source(),0,c_v_ix,data)
    # print(data)
    # print(c_v_ix)
    # exit(1)
    len_v = data[tdg.sink().id]['max']
    return len_v