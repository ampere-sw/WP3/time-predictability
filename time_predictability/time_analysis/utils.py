#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from functools import reduce
from typing import List, Dict
from time_predictability.meta_parallel.task import Task
from time_predictability.meta_parallel.tdg import TDG


def avg_makespan(tdg: TDG):
    sink: Task = tdg.sink()
    source: Task = tdg.source()
    # task.metrics.custom['begin'].append(result['execution_begin_time'])
    #       task.metrics.custom['end'].append(result['execution_end_time'])
    begins = source.metrics.get_measurement('begin').values
    ends = sink.metrics.get_measurement('end').values
    size = len(ends)
    if size == 0:
        id = tdg.amalthea_task_id if tdg.amalthea_task_id != None else tdg.id
        print("[TIME-ANALYSIS][Warning] Avg makespan from results could not be calculated for tdg",id," as the source and sink nodes are not annotated with the corresponding begin and end execution times.")
        return -1
    makespans = [ends[i] - begins[i] for i in range(0, size)]
    return reduce(lambda a,b: a+b, makespans) / size


def worst_makespan(tdg: TDG):
    sink: Task = tdg.sink()
    source: Task = tdg.source()
    # task.metrics.custom['begin'].append(result['execution_begin_time'])
    #       task.metrics.custom['end'].append(result['execution_end_time'])
    begins = source.metrics.get_measurement('begin').values
    ends = sink.metrics.get_measurement('end').values
    size = len(ends)
    if size == 0:
        id = tdg.amalthea_task_id if tdg.amalthea_task_id != None else tdg.id
        print("[TIME-ANALYSIS][Warning] Worst makespan from results could not be calculated for tdg",id," as the source and sink nodes are not annotated with the corresponding begin and end execution times.")
        return -1
    makespans = [ends[i] - begins[i] for i in range(0, size)]
    return max(makespans)

def critical_path(tdg:TDG, timing_data):
    if timing_data == None:
         timing_data = tdg.critical_path()
    current = tdg.sink()
    critical_path = [current]
    source = tdg.source()
    while current != source:
        res_ins = {task.id: timing_data[task.id] for task in current.ins}
        max_res = max(res_ins.keys(), key= (lambda key: res_ins[key].wcrt))
        current = tdg.tasks[max_res]
        critical_path.append(current)
    return reversed(critical_path)


def depth_aux(task:Task,data:Dict)->dict:
    data[task.id]['ins']+=1
    if data[task.id]['ins'] >= len(task.ins):
        data[task.id]['level'] = 1 + max([data[i.id]['level'] for i in task.ins])
        for cTask in task.outs:
            depth_aux(cTask,data)


def depth_data(tdg:TDG)->dict:
    data = {task: {'level':0,'ins':0} for task in tdg.tasks}
    source = tdg.source()
    for cTask in source.outs:
        depth_aux(cTask,data)
    return data


def depth(tdg:TDG)->int:
    return depth_data(tdg)[tdg.sink().id]['level']



def task_descendants(task:Task, data):
    data[task.id]['outs']+=1
    if data[task.id]['outs'] >= len(task.outs):
        for out in task.outs:
            data[task.id]['desc'].extend(val for val in data[out.id]['desc'] if val not in data[task.id]['desc'])
        for _in in task.ins:
            task_descendants(_in,data)


def task_ancestors(task:Task, data):
    data[task.id]['ins']+=1
    if data[task.id]['ins'] >= len(task.ins):
        for _in in task.ins:
            data[task.id]['anc'].extend(val for val in data[_in.id]['anc'] if val not in data[task.id]['anc'])
        for out in task.outs:
            task_ancestors(out,data)


def find_untainted_descendant(curr_task,mid_path,tdg,missing):
    for child in tdg.tasks[curr_task].outs:
        mid_path.append(child.id)
        untainted_children = [untainted.id for untainted in child.outs if untainted.id in missing]
        if untainted_children: 
            return untainted_children[0] #return first untainted descendant
        else:
            untainted_descendant = find_untainted_descendant(child.id,mid_path,tdg,missing)
            if untainted_descendant == None: #no untainted in this path, look for another path
                mid_path.remove(child.id)
            else:
                return untainted_descendant #return first untainted descendant
    return None


def build_path(curr_task:str,path:List[str],tdg:TDG,data:Dict,tainted:List[str], missing:List[str]):
    path.append(curr_task)
    tainted.append(curr_task)
    missing.remove(curr_task)
    untainted_children = [untainted.id for untainted in tdg.tasks[curr_task].outs if untainted.id in missing]
    if untainted_children:
        child_task = untainted_children[0] #right now i'm just considering the left-most child, nothing more complicated
        build_path(child_task,path,tdg,data,tainted,missing)
    else:
        mid_path = []
        untainted_node = find_untainted_descendant(curr_task,mid_path,tdg,missing)
        if untainted_node == None: #then curr_noe was the last untainted node in path
            return
        else:
            path.extend(mid_path)
            build_path(untainted_node,path,tdg,data,tainted,missing)

def max_parallelism(tdg: TDG,dep_data:Dict[str,dict]=None):
    if not dep_data:
        dep_data:Dict[str,dict] = depth_data(tdg)
    tainted = []
    missing = sorted(tdg.tasks.keys() ,key=lambda v: dep_data[v]['level'])
    par_paths = []
    while missing:
        path = []
        build_path(missing[0],path,tdg,dep_data,tainted,missing)
        par_paths.append(path)
    return len(par_paths)

def makespan(tdg:TDG):
    if any((task.static_thread == -1 for task in tdg.tasks.values())):
        raise Exception('makespan can only be calculated if a full mapping is provided')
    
    from time_predictability.simulation.scheduler import StaticThread_Master
    from time_predictability.simulation.simulator import Simulation, Simulator
    num_threads = 1 + max((task.static_thread for task in tdg.tasks.values())) #+1 because thread id starts at 0
    simulator: Simulator = Simulator(tdg,StaticThread_Master(num_threads),num_threads)
    simulation: Simulation = simulator.simulate()
    tdg.set_makespan(simulation.makespan)
    return tdg.makespan()


def depth_aux(task:Task,data:Dict)->dict:
    data[task.id]['ins']+=1
    if data[task.id]['ins'] >= len(task.ins):
        data[task.id]['level'] = 1 + max([data[i.id]['level'] for i in task.ins])
        for cTask in task.outs:
            depth_aux(cTask,data)


def depth_data(tdg:TDG)->dict:
    data = {task: {'level':0,'ins':0} for task in tdg.tasks}
    source = tdg.source()
    for cTask in source.outs:
        depth_aux(cTask,data)
    return data

    
