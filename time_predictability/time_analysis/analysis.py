#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from typing import Dict, List, Tuple
from time_predictability.meta_parallel.metrics.metrics import Measurement
from time_predictability.meta_parallel.app import App
from time_predictability.meta_parallel.task import Task, Task_RT
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.time_analysis import utils
from time_predictability.time_analysis.response_time.response_time import tied_wcrt_form_1, tied_wcrt_form_2, untied_wcrt
def wcets(tdg:TDG):
    return tdg.calc_tasks_wcet()

def pmcs(tdg:TDG):
    return tdg.calc_tasks_pmcs_metrics()

def calc_pmc_ratios(tdg:TDG,ratios: List[Tuple[str,str,str]]):
    return tdg.calc_tasks_pmcs_ratios(ratios)

def avg_task_times(tdg:TDG):
    return tdg.calc_tasks_avg_time()

def volume(tdg:TDG):
    return tdg.volume()

def critical_path_length(tdg:TDG):
    return tdg.critical_path()

def max_parallelism(tdg: TDG):
    return tdg.max_parallelism()

def depth(tdg:TDG):
    return tdg.depth()

def makespan(tdg:TDG):
    return utils.makespan(tdg)

def avg_makespan(tdg:TDG):
    return tdg.avg_makespan()

def worst_makespan(tdg:TDG):
    return tdg.worst_makespan()

def wcrt_untied(tdg:TDG,num_threads):
    tdg.set_wcrt_untied(untied_wcrt(tdg.volume(), tdg.critical_path(), tdg.max_parallelism(),num_threads))
    return tdg.wcrt_untied()

def wcrt_tied(tdg:TDG,num_threads):
    wcrt_tied_1 = tied_wcrt_form_1(tdg.volume(), tdg.critical_path(), tdg.max_parallelism(), tdg.depth(), num_threads)
    wcrt_tied_2 = tied_wcrt_form_2(tdg, tdg.volume(), tdg.max_parallelism(), num_threads)
    tdg.set_wcrt_tied(min(tdg.volume(),wcrt_tied_1,wcrt_tied_2))
    return tdg.wcrt_tied()

class TDGAnalysis:
    metrics_names = [
        'volume',
        'cpl',
        'max_par',
        'depth',
        'makespan',
        'avg_makespan',
        'worst_makespan',
        'wcrt'
    ]
    def __init__(self, num_threads=1):
        # self.tdg = tdg
        self.tasks = TaskAnalysisConfig()
        self.volume = True
        self.cpl = True  #critical path length
        self.max_par = True #max parallelism
        self.depth = True #maximum depth from source to sink
        self.makespan = False #only when a static_mapping is defined
        self.avg_makespan = True #calculated based on all the results in the TDG
        self.worst_makespan = True #calculated based on all the results in the TDG
        self.wcrt = True #based on existing formulas
        self.num_threads = num_threads
        self._clear_results = False

    def include(self, metrics:List[str]):
        self.set_attributes(metrics,True)

    def exclude(self, metrics:List[str]):
        self.set_attributes(metrics,False)
    
    def exclude_all(self):
        self.set_attributes(TDGAnalysis.metrics_names,False)
        self.set_attributes(TaskAnalysisConfig.metrics_names,False)
        self.tasks.ratios = []

    def clear_results(self):
        self._clear_results = True

    def add_ratio(self,name,dividend_pmc,divisor_pmc):
        self.tasks.add_ratio(name,dividend_pmc,divisor_pmc)

    def set_attributes(self, metrics:List[str], value:bool):
        for metric in metrics:
            if metric in TDGAnalysis.metrics_names:
                setattr(self, metric, value)
            elif metric in TaskAnalysisConfig.metrics_names:
                setattr(self.tasks, metric, value)
            else:
                raise Exception('Metric to include "'+metric+'" does not exist')

    def analyse_app(self, app:App):
        for tdg in app.tdgs:
            self.analyse_tdg(tdg)

    def analyse_tdg(self, tdg: TDG):
        if self.tasks.wcet:
            wcets(tdg)
        if self.tasks.pmc:
            pmcs(tdg)
        if self.tasks.avg_time:
            avg_task_times(tdg)
        if self.tasks.wcet:
            wcets(tdg)
        if self.tasks.ratios:
            calc_pmc_ratios(tdg,self.tasks.ratios)
        if self.volume:
            volume(tdg)
        if self.cpl:
            critical_path_length(tdg)
        if self.max_par:
            max_parallelism(tdg)
        if self.depth:
            depth(tdg)
        if self.makespan:
            makespan(tdg)
        if self.avg_makespan:
            avg_makespan(tdg)
        if self.worst_makespan:
            worst_makespan(tdg)
        if self.wcrt:
            wcrt_tied(tdg,self.num_threads)
            wcrt_untied(tdg,self.num_threads)

    def app_to_json(self, app: App):
        return app.to_json(not self._clear_results)

    def tdg_to_json(self, tdg: TDG):
        return tdg.to_json(not self._clear_results) #,extra

class TaskAnalysisConfig:
    metrics_names= ['wcet', 'pmc', 'avg_time', 'pmc_ratio']
    ratios = []
    def __init__(self):
        self.wcet = True,
        self.pmc  = True,
        self.avg_time = True
    
    def add_ratio(self,name,dividend_pmc,divisor_pmc):
        self.ratios.append((name,dividend_pmc,divisor_pmc))

def critical_path(tdg:TDG, timing_data):
    if timing_data == None:
         timing_data = tdg.latest_times()
    current = tdg.sink()
    critical_path = [current]
    source = tdg.source()
    while current != source:
        res_ins = {task.id: timing_data[task.id] for task in current.ins}
        max_res = max(res_ins.keys(), key= (lambda key: res_ins[key].wcrt))
        current = tdg.tasks[max_res]
        critical_path.append(current)
    return reversed(critical_path)