#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
from jsonschema import validate
from jsonschema import Draft202012Validator

END_TO_END_DEADLINE = "end_to_end_deadline"
TASKGRAPH_ID = "taskgraph_id"
AMALTHEA_TASK_ID = "amalthea_task_id"
RUNNABLE_ID = "runnable_id"
SPEC = "spec"
NODES = "nodes"
METRICS = "metrics"
INS = "ins"
OUTS = "outs"
REPLICAS = "replicas"
STATIC_THREAD = "static_thread"
RESULTS = "results"
CONSTRAINTS = "constraints"
PERIOD = "period"
DEADLINE = "deadline"
SCHEDULING_PARAMETERS = "scheduling_parameters"
RUNTIME = "runtime"

THREAD = "thread"
EXECUTION_BEGIN_TIME = "execution_begin_time"
EXECUTION_END_TIME = "execution_end_time"
EXECUTION_TOTAL_TIME = "execution_total_time"


COUNTERS = "counters"
AVG = "avg"
STDDEV = "stddev"
MAX = "max"
MIN = "min"
COUNT = "count"
TOT = "tot"
WCET = "wcet"
AVG_TIME = "avg_time"


VOLUME = 'volume'
CRITICAL_PATH = 'critical_path'
MAX_PARALLEL = 'max_parallel'
DEPTH = 'depth'
MAKESPAN = 'makespan'
AVG_MAKESPAN = 'avg_makespan'
WORST_MAKESPAN = 'worst_makespan'
WCRT_TIED = 'wcrt_tied'
WCRT_UNTIED = 'wcrt_untied'


class AppSchema:
    def validate_json(json_obj):
        
        validate(json_obj, AppSchema.config_schema())
    def config_schema():
        return {
            "type": "object",
            "additionalProperties": {"type": "array",
                "items": TdgSchema.config_schema()
            },
            "minProperties": 1, #for AMPERE, we only have one app per file
            "maxProperties": 1
        }


class TdgSchema:


    def validate_json(json_obj):
        validate(json_obj, TdgSchema.config_schema())
    
    def config_schema():
        return {
            "type": "object",
            "properties": {
                TASKGRAPH_ID: {"type": "integer"},
                NODES: TdgSchema.tasks_map_schema,
                #METRICS: TdgSchema.tdg_metrics_schema
            },
            "required": [TASKGRAPH_ID]#, NODES]
        }
    
    result_schema = {"type": "object",
        "properties": {
            THREAD: {"type": "integer"},
            EXECUTION_BEGIN_TIME: {"type": "integer"},
            EXECUTION_END_TIME: {"type": "integer"},
            EXECUTION_TOTAL_TIME: {"type": "integer"},
        },
        "additionalProperties": {"type": "number"}, #key is the name of the performance counter
        "required": [THREAD, EXECUTION_BEGIN_TIME,EXECUTION_END_TIME,EXECUTION_TOTAL_TIME]
    }

    pmc_metrics_schema = {"type": "object",
        "properties": {
            MAX: {"type":"number"},
            MIN: {"type":"number"},
            AVG: {"type":"number"},
            STDDEV: {"type":"number"},
            COUNT: {"type":"number"},
            TOT: {"type":"number"},
        }
    }

    task_metrics_schema = {"type": "object",
        "properties": {
            WCET: {"type": "integer"},
            AVG_TIME: {"type": "number"},
            COUNTERS: {"type":"object", #key is the name of the performance counter
                "additionalProperties": pmc_metrics_schema
            }
        }
    }

    task_schema = {"type": "object",
        "properties": {
            INS: {"type": "array", "items": {"type": "string"}},
            OUTS: {"type": "array", "items": {"type": "string"}},
            REPLICAS: {"type": "array", "items": {"type": "string"}},
            STATIC_THREAD: {"type": "integer"},
            RESULTS: {"type": "array", "items": result_schema},
            METRICS: task_metrics_schema
        },
        "required": [INS, OUTS]
    }

    tasks_map_schema = {"type": "object",
        "additionalProperties": task_schema
    }

    tdg_metrics_schema ={"type": "object",
        "properties": {
            VOLUME: {"type":"integer"},
            CRITICAL_PATH: {"type":"integer"},
            MAX_PARALLEL: {"type":"integer"},
            DEPTH: {"type":"integer"},
            MAKESPAN: {"type":"integer"},
            AVG_MAKESPAN: {"type":"number"},
            WORST_MAKESPAN: {"type":"integer"},
            WCRT_TIED: {"type":"number"},
            WCRT_UNTIED: {"type":"number"},
        },
        "additionalProperties": {"type": "number"}, #key is the name of the metric
    }