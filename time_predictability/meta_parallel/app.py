

import json
from typing import Dict, List
from time_predictability.meta_parallel.tdg_schema import *

from time_predictability.meta_parallel.tdg import TDG

END_TO_END_DEADLINE = "end_to_end_deadline"

class App:
    def __init__(self, name:str, tdgs:List[TDG]):
        self.name:str = name
        self.tdgs = tdgs
        self.end_to_end_deadline = -1
    # from a json file that contains both TDG format and experimental results
    # automatically imports measurements
    def read_json(json_path: str, validate = False) -> 'App':

        # json_file = open(json_path)
        with open(json_path,'r') as json_file:
            tdg_results:Dict = json.load(json_file)
            if validate:
                TdgSchema.validate_json(tdg_results)
        end_to_end_deadline = tdg_results.pop(END_TO_END_DEADLINE,-1)
        name,contents = next(iter(tdg_results.items())) #in AMPERE we only deal with one app per file
        # contents = tdg_results[name]
        tdgs = [TDG.from_json(tdg_result,False) for tdg_result in contents]
        app = App(name,tdgs)
        app.end_to_end_deadline = end_to_end_deadline
        return app 
    
    def read_dots(dot_paths: List[str], appName: str = 'app') -> 'App':
        tdgs = [TDG.read_dot(dot_file) for dot_file in dot_paths]
        return App(appName,tdgs)
    
    def to_json(self, keep_results: bool=True, keep_metrics: bool=True) -> Dict: 
        
        tdgs = [tdg.to_json(keep_results,keep_metrics) for tdg in self.tdgs]
        return {self.name: tdgs, END_TO_END_DEADLINE: self.end_to_end_deadline}

    def get_tdg(self, id: str):
        for tdg in self.tdgs:
            if tdg.id == id:
                return tdg
        return None
    
    def get_tdg_by_amalthea_id(self, name: str):
        for tdg in self.tdgs:
            if tdg.amalthea_task_id == name:
                return tdg
        return None
    
    def get_mappings(self)->Dict:
        return {tdg.id: tdg.get_mapping() for tdg in self.tdgs}
        