#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from collections import defaultdict
import json
import numpy as np
from typing import List, Dict, Tuple, Union
from copy import deepcopy
from pydotplus import Dot, Subgraph, Edge, graph_from_dot_file
from time_predictability.meta_parallel.task import Task, Task_RT
from time_predictability.meta_parallel.metrics.metrics import Measurement, Metric
from time_predictability.meta_parallel.utils import tdg_to_dot
from time_predictability.meta_parallel.tdg_schema import *
import time_predictability.meta_parallel.tdg_schema as tdg_schema

class TDG:
  default_tdg_metrics = ['volume','critical_path','max_parallel','depth','makespan','avg_makespan', 'worst_makespan','wcrt_tied','wcrt_untied']
  def __init__(self, id:str, tasks:Dict[str,Task], amalthea_task_id:str = None):
    self.id:str = id
    self.amalthea_task_id: str  = amalthea_task_id
    self.tasks:Dict[str,Task] = tasks
    self.normalized = False
    self.normalize_source_sink()
    self.tdg_metrics:Dict[str,int or float] = {}
    self.json = None
    self.dot = None
    self.ins = []
    self.outs = []
    self.constraints = {
        "period": -1,
        "deadline": -1
    }
    self.scheduling_parameters = []
    # self._volume = -1
    # self._critical_path_length = -1
    # self._max_parallelism = -1
    # self._depth = -1
    # self._makespan = -1
    # self._avg_makespan = -1
    # self._worst_makespan = -1
    # self._wcrt_tied = -1
    # self._wcrt_untied = -1
    

  def set_path(self, path:str):
    self.path = path

  def set_relative_path(self, relative_path:str):
    self.relative_path = relative_path

  def __set_dot(self, dot:Dot):
    self.dot = dot

  def __set_json(self, json:Dict):
    self.json = json

  def read_dot(dot_path: str):
    dot: Dot = graph_from_dot_file(dot_path)

    subg: Subgraph = dot.get_subgraphs()[0]

    tdg_nodes:list = subg.get_nodes()
    tdg_nodes

    tasks:Dict[str,Task] = {node.get_name(): 
                            Task(node.get_name(),False,node.get_attributes()['label'] if 'label' in node.get_attributes() else None) 
                            for node in tdg_nodes
                            } #dict()#:List[Task] = [None] * total_nodes
    

    # replicas: Dict[str,List[Task]] = defaultdict(list)
    # for task in tasks.values():
    #   curr_replicas: List[Task] = replicas[task.runnable_id]
    #   for replica in curr_replicas:
    #     replica.replicas.append(task)
    #   task.replicas.extend(curr_replicas)
    #   replicas[task.runnable_id].append(task)

    tasks_with_run_id = [task for task in tasks.values() if task.runnable_id != None]
    replicas: Dict[str,List[Task]] = defaultdict(list)
    for task in tasks_with_run_id:
      if task.runnable_id:
        replicas[task.runnable_id].append(task)

    for task in tasks_with_run_id:
      task.replicas = [replica for replica in replicas[task.runnable_id] if replica != task]


    tdg_edges:list = dot.get_edges()

    edge: Edge
    #assuming ids as keys
    for edge in tdg_edges:
        source = edge.get_source()
        dest = edge.get_destination()
        if source not in tasks:
            tasks[source] = Task(source)
        if dest not in tasks:
            tasks[dest] = Task(dest)
        tasks[source].addOut(tasks[dest])
        tasks[dest].addIn(tasks[source])


    id = 'tdg',
    amalthea_id = None
    tdg_attrs: Dict = subg.get_attributes()
    if 'label' in tdg_attrs:
      label:str = tdg_attrs['label']
      first_ = label.index("_")
      second_ = label.index("_",first_+1)
      id = label[first_+1:second_]
      amalthea_id = label[second_+1:]
      if amalthea_id.startswith('Node_'):
        amalthea_id = amalthea_id[len('Node_'):]
    tdg = TDG(id,tasks,amalthea_id)
    tdg.__set_dot(dot)
    return tdg

  # # from a json file that contains both TDG format and experimental results
  # # automatically imports measurements
  # def read_json(json_path: str, id:str ="TDG") -> Union['TDG', Dict['str','TDG']]:

  #   json_file = open(json_path)
  #   tdg_results:Dict = json.load(json_file)
  #   json_file.close()
  #   if multi_tdgs:
  #     return  {k: TDG.from_json(tdg_result,k) for k,tdg_result in tdg_results.items}
  #   return TDG.from_json(tdg_results,id)
  
  # from a json that contains both TDG format and experimental results
  # automatically imports measurements
  def from_json(tdg_json: Dict, validate:bool = True) -> 'TDG':
    """
    supports json in the format: {taskgraph_id,nodes:{id:{ins,outs[,results,metrics]}}[],metrics]}
    """
    if validate:
      TdgSchema.validate_json(tdg_json)
    id = tdg_json[TASKGRAPH_ID]
    nodes = tdg_json[NODES]
    node_ids = nodes.keys()
    tasks = dict(zip(node_ids, [Task(k) for k in node_ids]))
    default_props = ['thread', 'execution_begin_time','execution_end_time','execution_total_time']
    task: Task
    for k,task in tasks.items():
      node = nodes[k]
      task.ins = [tasks[i] for i in node[INS]]
      task.outs = [tasks[o] for o in node[OUTS]]
      if RUNNABLE_ID in node:
          task.runnable_id = node[RUNNABLE_ID]
      if SPEC in node:
          task.spec = node[SPEC] 
      if REPLICAS in node:
        task.replicas = [tasks[r] for r in node[REPLICAS]]
      if STATIC_THREAD in node:
        task.static_thread = node[STATIC_THREAD]
      
      if RESULTS in node:
        results = node[RESULTS]
        task.metrics.new_measurement('begin')
        task.metrics.new_measurement('end')
        task.metrics.new_measurement('thread')
        for result in results:
          task.metrics.exec_time.append(result[EXECUTION_TOTAL_TIME])
          if EXECUTION_BEGIN_TIME in result:
            task.metrics.custom_measurements['begin'].append(result[EXECUTION_BEGIN_TIME])
          if EXECUTION_END_TIME in result:
            task.metrics.custom_measurements['end'].append(result[EXECUTION_END_TIME])
          if THREAD in result:
            task.metrics.custom_measurements['thread'].append(result[THREAD])
          for perf_counter in result.keys()-default_props:
            if perf_counter not in task.metrics.pmc_measurements:
              task.metrics.pmc_measurements[perf_counter] = Measurement(perf_counter)
            task.metrics.pmc_measurements[perf_counter].append(result[perf_counter])
      
      if METRICS in node:
        metrics: Dict[str,int or float or object] = node[METRICS]
        for metric,value in metrics.items():
          if metric == tdg_schema.WCET:
            task.metrics.set_wcet(value)
          elif metric == tdg_schema.AVG_TIME:
            task.metrics.set_avg_time(value)
          elif metric == tdg_schema.COUNTERS:
            for counter, metrics in value.items():
              task.metrics.set_pmc_metrics(counter,metrics)
          else:
            task.metrics.set_custom_metric(metric,value)

    tdg = TDG(id,tasks)
    tdg.new_dot() #forces creation of a new dot
    tdg.__set_json(tdg_json)

    if AMALTHEA_TASK_ID in tdg_json:
      tdg.amalthea_task_id = tdg_json[AMALTHEA_TASK_ID]

    if INS in tdg_json:
      tdg.ins = [_in for _in in tdg_json[INS]]
    
    if OUTS in tdg_json:
      tdg.outs = [_out for _out in tdg_json[OUTS]]

    if CONSTRAINTS in tdg_json:
      tdg.constraints[PERIOD] = tdg_json[CONSTRAINTS][PERIOD]
      tdg.constraints[DEADLINE] = tdg_json[CONSTRAINTS][DEADLINE]

    if SCHEDULING_PARAMETERS in tdg_json:
      tdg.scheduling_parameters = [sched_params.copy() for sched_params in tdg_json[SCHEDULING_PARAMETERS]]

    if METRICS in tdg_json:
      for metric,value in tdg_json[METRICS].items():
        tdg.set_metric(metric,value)
    return tdg
# globalInfo: Dict[str,Dict[str,any]] = {}, 
# extraInfo: Dict[str,Dict[str,any]] = {},
  def to_json(self,  keep_results: bool=True, keep_metrics: bool=True) -> Dict:

    #first build tdg with things that are input and we don't mess around with
    if self.json:
      res:dict = deepcopy(self.json)
      if not keep_results or not keep_metrics:
        for k,t in self.tasks.items():
          if t.is_virtual:
            continue
          task = res[NODES][k]
          if not keep_results:
            task[RESULTS] = []
          if not keep_metrics:
            task[METRICS] = {}
      if not keep_metrics:
        res[METRICS] = {}
          # if RESULTS in task:
          #   task.pop(RESULTS)
    else:
      res:dict = {TASKGRAPH_ID: self.id, NODES:{}}
      task:Task
      for k, task in self.tasks.items():
        if task.is_virtual:
          continue
        node = {
          INS: [_in.id for _in in task.ins if not _in.is_virtual],
          OUTS: [_out.id for _out in task.outs if not _out.is_virtual],          
        }
        res[NODES][task.id] =  node
        if task.has_replicas():
          node[REPLICAS] = [rep.id for rep in task.replicas]
        node[RESULTS] = []
        if keep_results:
          begins = task.metrics.custom_measurements['begin'] if 'begin' in task.metrics.custom_measurements else []
          num_begins = len(begins)
          ends = task.metrics.custom_measurements['end'] if 'end' in task.metrics.custom_measurements else []
          num_ends = len(ends)
          threads = task.metrics.custom_measurements['thread'] if 'thread' in task.metrics.custom_measurements else []
          num_threads = len(threads)
          for i in range(task.metrics.exec_time.length()):
            result = {
              EXECUTION_TOTAL_TIME:task.metrics.exec_time.values[i],
            }
            if i < num_begins:
              result[EXECUTION_BEGIN_TIME] =task.metrics.custom_measurements['begin'].values[i]
            if i < num_ends:
              result[EXECUTION_END_TIME] = task.metrics.custom_measurements['end'].values[i],
            if i < num_threads:
              result[THREAD] = threads.values[i]
            for pc,vals in task.metrics.pmc_measurements.items():
              result[pc] = vals[pc][i]
            node[RESULTS].append(result)

    if self.amalthea_task_id != None:
      res[AMALTHEA_TASK_ID] = self.amalthea_task_id

    res[INS] = [_in for _in in self.ins]
    res[OUTS] = [_out for _out in self.outs]
    res[CONSTRAINTS] = {
      PERIOD: self.constraints[PERIOD],
      DEADLINE: self.constraints[DEADLINE]
    }

    res[SCHEDULING_PARAMETERS] = [sched_params.copy() for sched_params in self.scheduling_parameters]

    #adding wcet, static_thread and potential extra information
    task:Task
    for id, task in self.tasks.items():
      if task.is_virtual:
        continue
      if task.spec != None:
        res[NODES][id][SPEC] = task.spec
      if task.runnable_id != None:
        res[NODES][id][RUNNABLE_ID] = task.runnable_id
      if task.static_thread > -1:
        res[NODES][id][STATIC_THREAD] = task.static_thread
      if keep_metrics and task.metrics.has_any_metric():
        if METRICS not in res[NODES][id]:
          res[NODES][id][METRICS] = {}
        metrics = res[NODES][id][METRICS]
        #Only put wcet if it was previously requested to calculate
        if task.metrics.wcet_value:
          metrics[WCET] = task.metrics.wcet()
        if task.metrics.avg_time_value:
          metrics[AVG_TIME] = task.metrics.avg_time()
        if task.metrics.has_pmc_metrics():
          counters = {}
          metrics[COUNTERS] = counters
          for pmc,pm_metrics in task.metrics.pmc_metrics.items():
            counters[pmc] = {v.name: v.value for v in pm_metrics.values()}

        if task.metrics.has_custom_metrics():
          for k,v in task.metrics.get_custom_metrics().items():
            metrics[k] = v
      # if id in extraInfo:
      #   for k,v in extraInfo[id].items():
      #     res[id][k] = v
    
    if keep_metrics and self.tdg_metrics:
      if METRICS not in res:
        res[METRICS] = {}

      for prop,value in self.tdg_metrics.items():
        if value is not None:
          res[METRICS][prop] = value

    # for k,v in globalInfo.items():
    #   res[k] = v
    return res


  def new_dot(self) -> None:
    self.dot = tdg_to_dot(self)

  def addMeasurements(self, all_measurements: list) -> None:
    """
    Add extra measurements to the TDG if the measurements were stored apart from the TDG
    """

    for taskMeasurement  in [tasksMeasurements for eachMeasurement in all_measurements for tasksMeasurements in eachMeasurement]:
        
        task = self.tasks[taskMeasurement["id"]]
        # task.metrics.exec_time.values.extend([result["exec_time"] for result in taskMeasurement["results"]])
        for result in taskMeasurement["results"]:
            for perf_counter in result:
                if perf_counter == "exec_time":
                    task.metrics.exec_time.append(result[perf_counter])
                else:
                    if perf_counter not in task.metrics.perf_counters:
                        task.metrics.perf_counters[perf_counter] = Measurement(perf_counter)
                    task.metrics.perf_counters[perf_counter].append(result[perf_counter])

  def rename_perf_counters(self,names:Dict) -> None:
    for task in self.tasks.values():
      task.metrics.rename(names)
  
  def task_list(self) -> List[Task]:
    return self.tasks.values()

  def sources(self) -> List[Task]:
    return [root for root in self.tasks.values() if not root.ins]

  def sinks(self) -> List[Task]:
    return [sink for sink in self.tasks.values() if not sink.outs]

  def source(self) -> Task:
    if not hasattr(self,'_source'):
      self.normalize_source_sink()
    return self._source #if hasattr(self,'_source') else self.sources()

  def sink(self) -> Task:
    if not hasattr(self,'_sink'):
      self.normalize_source_sink()
    return self._sink #if hasattr(self,'_sink') else self.sinks()

  def calc_tasks_wcet(self) -> Dict[str,int]:
    """
    Calculates the wcet for the tasks that yet do not have the wcet annotated. 
    If wcet is not set for a task, the function "wcet()" automatically calculates (from the results) and stores the wcet for that task
    Returns a dictionary mapping a task id to its wcet
    """
    return {id: task.metrics.wcet() for id, task in self.tasks.items()}

  def calc_tasks_avg_time(self) -> Dict[str,int]:
    return {id: task.metrics.avg_time() for id, task in self.tasks.items()}

  def calc_tasks_pmcs_metrics(self) -> Dict[str,Dict[str,int or float]]:
    """
    Calculates standard metrics for each pmc in the results section of each the tasks.
    Returns a dictionary mapping each pmc to the metrics
    """
    return {id: task.metrics.calc_all_pmc_metrics() for id, task in self.tasks.items()}

  def calc_tasks_pmcs_ratios(self,ratios: List[Tuple[str,str,str]]):
    for id, task in self.tasks.items():
      for tuple in ratios:
        task.metrics.calc_pmcs_ratio(tuple[0],tuple[1],tuple[2])

  def normalize_source_sink(self) -> None:
    """
    Sanitizes the tdg as a single source-sink structure.
    
    For source:
    1. Adds a new node (ergo source) if more than one source task (i.e. empty ins)
    2. Adds those tasks as "outs" of source (and source as "ins" for each task)
    3. If measurements exist in 'original' sources, create measurements, for each measurement sample:
      3.1. execution time = always 0
      3.2. begin time = minimum time between all times of all original source tasks
      3.3. end time = begin time (as it should always have no execution time)
    
    For sink:
    1. Adds a new node (ergo sink) if more than one sink task (i.e. empty outs)
    2. Adds those tasks as "ins" of sink (and sink as "outs" for each task)
    3. If measurements exist in 'original' sinks, create measurements, for each measurement sample:
      3.1. execution time = always 0
      3.2. begin time = maximum time between all times of all original sink tasks
      3.3. end time = begin time (as it should always have no execution time)
    """
    sources:List[Task] = self.sources()
    if len(sources) > 1:
      source = Task('source',is_virtual = True)
      source.metrics.exec_time.append(0)
      begins = [s.metrics.get_measurement('begin').values for s in sources if s.metrics.has_measurement('begin')]
      shape = np.shape(begins)
      if len(shape) > 1:
        begin = source.metrics.new_measurement('begin')
        end = source.metrics.new_measurement('end')
        for i in range(0,shape[1]):
          min_begin = min([v[i] for v in begins ])
          begin.append(min_begin)
          end.append(min_begin)

      self.tasks[source.id] = source
      source.outs = sources
      for old_source in sources:
        old_source.addIn(source)
      self._source = source
    else:
      self._source = sources[0]

    sinks = self.sinks()
    if len(sinks) > 1:
      sink = Task('sink',is_virtual = True)
      sink.metrics.exec_time.append(0)
      ends = [s.metrics.get_measurement('end').values for s in sinks if s.metrics.has_measurement('end')]
      shape = np.shape(ends)
      if len(shape) > 1:
        begin = sink.metrics.new_measurement('begin')
        end = sink.metrics.new_measurement('end')
        for i in range(0,shape[1]):
          max_end = max([v[i] for v in ends])
          begin.append(max_end)
          end.append(max_end)

      self.tasks[sink.id] = sink
      sink.ins = sinks
      for old_sink in sinks:
        old_sink.addOut(sink)
      self._sink = sink
    else:
      self._sink = sinks[0]
    self.normalized = True
        
  def volume(self) -> int:
    if "volume" not in self.tdg_metrics:
      self.tdg_metrics["volume"] = sum(task.metrics.wcet() for task in self.task_list())
    return self.tdg_metrics["volume"]
  
  def critical_path(self) -> int:
    if "critical_path_length" not in self.tdg_metrics:
      task_times: Dict[str,Task_RT] = self.latest_times()
      sink_task: Task = self.sink()
      self.tdg_metrics["critical_path_length"] = task_times[sink_task.id].latest_end
    return self.tdg_metrics["critical_path_length"]

  def max_parallelism(self) -> int:
    if "max_parallelism" not in self.tdg_metrics:
      import time_predictability.time_analysis.utils as analysis_utils
      self.tdg_metrics["max_parallelism"] = analysis_utils.max_parallelism(self)
    return self.tdg_metrics["max_parallelism"]

  def set_makespan(self,value):
    self.tdg_metrics["makespan"] = value
  
  def makespan(self):
    return self.tdg_metrics["makespan"]

  def calc_makespan_if_mapping(self):
    task: Task
    threads = [task.static_thread for task in self.task_list() if not task.is_virtual]
    if -1 in threads:
      return None
    max_threads = max(threads)
    exec_times = [0]*(max_threads+1)
    for task in self.task_list():
      exec_times[task.static_thread]+= task.metrics.wcet()
    makespan = max(exec_times)
    self.set_makespan(makespan)
    return makespan

    

  def set_avg_makespan(self,value):
    self.tdg_metrics["avg_makespan"] = value
  
  def avg_makespan(self):
    if "avg_makespan" not in self.tdg_metrics:
      import time_predictability.time_analysis.utils as analysis_utils
      self.tdg_metrics["avg_makespan"] = analysis_utils.avg_makespan(self)
    return self.tdg_metrics["avg_makespan"]
  
  def set_worst_makespan(self,value):
    self.tdg_metrics["worst_makespan"] = value
  
  def worst_makespan(self):
    if "worst_makespan" not in self.tdg_metrics:
      import time_predictability.time_analysis.utils as analysis_utils
      self.tdg_metrics["worst_makespan"] = analysis_utils.worst_makespan(self)
    return self.tdg_metrics["worst_makespan"]

  def set_wcrt_tied(self,value):
    self.tdg_metrics["wcrt_tied"] = value
  
  def wcrt_tied(self):
    return self.wcrt_tied

  def set_wcrt_untied(self,value):
    self.tdg_metrics["wcrt_untied"] = value
  
  def wcrt_untied(self):
    return self.wcrt_untied
  
  def depth(self) -> int:
    if "depth" not in self.tdg_metrics:
      import time_predictability.time_analysis.utils as analysis_utils
      self.tdg_metrics["depth"] = analysis_utils.depth(self)
    return self.tdg_metrics["depth"]

  def latest_times(self) -> dict:
    data = {}
    if not self.normalized:
      for source in self.sources():
        source.latest_start_end(data)
    else:
      self.source().latest_start_end(data)

    return data

  def set_static_threads(self,mapping: Dict[str,int], makespan: int):
    for id, thread in mapping.items():
        self.tasks[id].static_thread = thread
    self.tdg_metrics["makespan"] = makespan

  def get_mapping(self):
    return {id: task.static_thread for id, task in self.tasks.items()}
  
  def set_metric(self,metric:str,value):
    self.tdg_metrics[metric] = value

  def get_task_by_runnable_name(self, name:str) -> Task:
    task:Task
    for task in self.task_list():
      if task.runnable_id == name:
        return task
    return None