#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from typing import List, Dict
from time_predictability.meta_parallel.metrics.metrics import Metrics



class Task:
  """
  Class representing a Task in the TDG
  """
  def __init__(self, id, is_virtual=False, runnable_id:str=None):
    self.id:str = id
    self.spec = None
    if runnable_id != None:
      if "_" in runnable_id:
        last_underscore_index = runnable_id.rindex("_")
        suffix = runnable_id[last_underscore_index+1:]
        if suffix == "GPU" or suffix == "FPGA":
          self.spec = suffix
          runnable_id = runnable_id[:last_underscore_index] 
      self.runnable_id: str  = runnable_id
    else:
      self.runnable_id = None
    
    self.ins:List[Task]  = []
    self.outs:List[Task] = []
    self.replicas:List[Task] = []
    self.metrics:Metrics = Metrics()
    #true if this task was added to force single source/sink structure
    self.is_virtual:bool = is_virtual
    self.static_thread: int = -1

  def hasMetrics(self)->bool:
    return self.metrics.has_measurements

  def addIn(self, inTask):
    self.ins.append(inTask)

  def addOut(self, outTask):
    self.outs.append(outTask)

  def addReplica(self, replica):
    self.replicas.append(replica)

  def has_replicas(self):
    return len(self.replicas) != 0

  def latest_start_end(self, data:Dict, current_time = 0):
    """
    Calculates the latest start and latest end of the Task. 
    The latest start is the latest end between all in Tasks (which comes from current_time).
    The latest end will be the latest start + WCET of the Task.
    Does not return anything, but it populates the "data" parameter with the updated results.
    The latest end of the Sink task will represent the critical path length.
    """
    if not self.id in data:
      data[self.id]:Task_RT = Task_RT(self.metrics.wcet())
   
    dataRef:Task_RT = data[self.id]
    dataRef.latest_start = max(dataRef.latest_start,current_time)
    dataRef.latest_end = dataRef.latest_start+dataRef.wcet
    if self.ins:
      dataRef.ins += 1

    if(dataRef.ins >= len(self.ins)):
      for task_child in self.outs:
        task_child.latest_start_end(data, dataRef.latest_end)

class Task_RT:
  """
  Class used to calculate latest start/end of a Task. 
  The main objective of this is to find the critical path length.
  """
  def __init__(self, wcet):
      self.wcet = wcet
      self.ins = 0
      self.latest_start = 0
      self.latest_end = 0,