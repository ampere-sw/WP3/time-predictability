#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from enum import Enum
import os
from pydotplus import Dot, Subgraph, Node, Edge

class EventChainMode(Enum):
    LATENCY_CONSTRAINT = 0
    EVENT_CHAIN = 1
base_dir_name = os.path.dirname(__file__)
libs_location = os.path.join(base_dir_name,'libs')


def annotateAmaltheaModel(amxmi_file: str,tdg_file: str, output: str):
    cmd = 'java -jar '+libs_location+'/amaltheaAnnotator.jar SET -a "'+amxmi_file+'" -t "'+tdg_file + '" -o "'+ output + '"'
    print('running command: '+cmd)
    os.system(cmd)

def setAmxmiInfo(amxmi_file: str,tdg_file: str,eventChainMode: EventChainMode, eventChainName: str, output: str):
    cmd = 'java -jar '+libs_location+'/amaltheaAnnotator.jar GET -a "'+amxmi_file+'" -t "'+tdg_file + '" -o "'+ output + '"'
    if eventChainMode == EventChainMode.LATENCY_CONSTRAINT:
        cmd += ' -l "'+eventChainName+'"'
    elif eventChainMode == EventChainMode.EVENT_CHAIN:
        cmd += ' -c "'+eventChainName+'"'
    print('running command: '+cmd)
    os.system(cmd)

colors = [
    "aquamarine3",
    "blue2",
    "crimson",
    "chartreuse",
    "darkorchid3",
    "darkgoldenrod1",
    "darkgreen",
    "chocolate",
    "dimgray",
    "indianred2",
    "midnightblue",
    "rosybrown2",
    "wheat3",
    "black",
    "azure3",
    "antiquewhite2",
    "darkolivegreen",
    "darkorange1",
    "darksalmon",
    "cyan4",
]

MAX_COLORS = 100 + len(colors)

def grays():
    for unit in range(0,10):
        for decimal in range(0,100,10):
            yield "gray"+str(decimal + unit)

def colors_gen():
    for color in colors:
        yield color
    for gray in grays():
        yield gray

def single_color(color):
    while True:
        yield color

def save_png(dot,name):
    dot.write_png(name)


def tdg_to_dot(tdg,data=None):
    tasks = tdg.tasks
    ks = tasks.keys()
    length = len(tasks)
    color = colors_gen() if length <= MAX_COLORS else single_color(colors[0])

    g:Dot = Dot()
    g.set_type('digraph')
    g.set_name("TDG_"+str(tdg.id))
    g.set_compound('true')

    sg:Subgraph = Subgraph('cluster_0')
    sg.set_label('TDG_0')
    g.add_subgraph(sg)

    for k in ks:
        node = Node(k,color = next(color),style='bold')
        if data and k in data:
            if type(data[k]) is dict:
                label = _gen_table(k, **data[k])
            else:
                label = _gen_table(k, dummy=data[k])
            node.obj_dict["attributes"]["label"] = label
        sg.add_node(node)
        task = tasks[k]
        for dest in task.outs:
            g.add_edge(Edge(k,dest.id))
    return g


def _gen_head(name):
   return "<tr><td colspan='2'>"+str(name)+"</td></tr>"
def _gen_row(key, value):
   if key == 'dummy':
        return "<tr><td colspan='2'>"+str(value) +"</td></tr>"    
   return "<tr><td>"+str(key).replace("_"," ")+"</td><td>"+str(value) +"</td></tr>"

def _gen_table(name, **kwargs):
    str = "<<table>"
    str += "\n\t"+_gen_head(name)
    for k in kwargs:
        str += "\n\t"+_gen_row(k,kwargs.get(k))
    str +="</table>>"
    return str