#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import statistics
from typing import Callable, Dict, List
from functools import reduce

from time_predictability.meta_parallel.tdg_schema import AVG, COUNT, MAX, MIN, STDDEV, TOT

class Measurement:
  
  def __init__(self, name:str): 
    self.name = name
    self.values = []
  def has_measurements(self)->bool:
    return bool(self.values)

  def append(self,value):
    self.values.append(value)
  def max(self):
    return max(self.values)
  def min(self):
    return min(self.values)
  def total(self):
    return reduce(lambda a,b: a+b, self.values)
  def stddev(self):
    return statistics.stdev(self.values)
  def median(self):
    return statistics.median(self.values)
  def avg(self):
    return statistics.mean(self.values)

  def distribution(self):
    return statistics.NormalDist(self.values)

  def length(self):
    return len(self.values)

class Metric:
  def __init__(self, name:str, value: int or float):
    self.name = name
    self.value = value

class Metrics:
  def __init__(self):
    self.exec_time:Measurement = Measurement('exec_time')
    self.pmc_measurements:Dict[str,Measurement] = {}
    self.pmc_metrics:Dict[str,Dict[str,Metric]] = {}
    self.custom_measurements:Dict[str,Measurement] = {}
    self.custom_metrics:Dict[str,Metric] = {}
    self.wcet_value = None
    self.avg_time_value = None
  
  def rename(self,names:dict)->None:
    self.pmc_measurements = {names[k] if k in names else k: v for k,v in self.pmc_measurements.items()}
  
  def wcet(self)->int:
    if not self.wcet_value:
      self.wcet_value = self.exec_time.max()
    return self.wcet_value

  def set_wcet(self, wcet: int)->int:
    self.wcet_value = wcet

  def avg_time(self)->int:
    if not self.avg_time_value:
      self.avg_time_value = self.exec_time.avg()
    return self.avg_time_value

  def set_avg_time(self, avg_time: int)->int:
    self.avg_time_value = avg_time

  def has_measurements(self:'Metrics') -> bool:
    return self.exec_time.has_measurements() | bool(self.pmc_measurements) | bool(self.custom_measurements)
  
  def has_measurement(self:'Metrics',name) -> bool:
    return name in self.custom_measurements

  def new_measurement(self,name)->None:
    self.custom_measurements[name] = Measurement(name)
    return self.custom_measurements[name]
  
  def get_measurement(self,name)->Measurement:
    return self.custom_measurements[name]
  
  def get_metric(self,name)->Metric:
    return self.custom_metrics[name]
  
  def has_metric(self, name):
    return name in self.custom_metrics


  def get_or_calc_pmc_metrics(self,name) -> Dict[str,Metric]:
    if name in self.pmc_metrics:
      return self.pmc_metrics[name]
    
    if name not in self.pmc_measurements:
      raise Exception("Performance counter",name,"does not exist in the measurements")
    pmc_measurement: Measurement = self.pmc_measurements.get(name)
    metrics = {
      AVG: Metric(AVG,pmc_measurement.avg()),
      MAX: Metric(MAX,pmc_measurement.max()),
      MIN: Metric(MIN,pmc_measurement.min()),
      STDDEV: Metric(STDDEV,pmc_measurement.stddev()),
      TOT: Metric(TOT,pmc_measurement.total()),
      COUNT: Metric(COUNT,pmc_measurement.length())
    }
    self.pmc_metrics[name] = metrics
    return metrics

  def has_pmc_metrics(self)-> bool:
    return bool(self.pmc_metrics)

  def calc_all_pmc_metrics(self) -> Dict[str,Dict[str,int or float]]:
    res = {}
    for pmc in self.pmc_measurements:
      calced = self.get_or_calc_pmc_metrics(pmc)
      res[pmc] = {k: v.value for k,v in calced.items()}
    return self.pmc_metrics
  
  def set_pmc_metrics(self, name, metrics: Dict[str,int or float]):
    self.pmc_metrics[name] = {k: Metric(k,value) for k,value in metrics.items()}

  def get_pmc_metrics(self, name):
    return {k: value.value for k,value in self.pmc_metrics[name].items()}
  
  def get_all_pmc_metrics(self, name):
    return {k: value.value for k,value in self.pmc_metrics[name].items()}

  def set_custom_metric(self, name, value):
    self.custom_metrics[name] = value
  
  def get_custom_metric(self, name):
    return self.custom_metrics[name]
  
  def get_custom_metrics(self):
    return self.custom_metrics

  def has_custom_metrics(self):
    return bool(self.custom_metrics)

  def custom_metrics_names(self):
    return self.custom_metrics.keys()

  def calc_custom_metric(self,name, calculator: Callable[[Measurement],int or float]):
    measurement = self.custom_measurements[name]
    value = calculator(measurement)
    self.set_custom_metric(name,value)
  
  def _ratio_of(dividend_measurement,divisor_measurement):
    return {
      AVG: _non_zero_div(dividend_measurement.avg(),divisor_measurement.avg()),
      MAX: _non_zero_div(dividend_measurement.max(),divisor_measurement.max()),
      MIN: _non_zero_div(dividend_measurement.min(),divisor_measurement.min()),
      STDDEV: _non_zero_div(dividend_measurement.stddev(),divisor_measurement.stddev()),
      TOT: _non_zero_div(dividend_measurement.total(),divisor_measurement.total()),
      COUNT: _non_zero_div(dividend_measurement.length(),divisor_measurement.length())
    }

  def calc_pmcs_ratio(self, name, dividend_pmc, divisor_pmc):
    self.calc_custom_pmcs_metric(name,
      [dividend_pmc,divisor_pmc], 
      lambda pmcs: Metrics._ratio_of(pmcs[dividend_pmc],pmcs[divisor_pmc]))
    # dividend_measurement: Measurement = self.pmc_measurements.get(dividend_pmc)
    # divisor_measurement: Measurement = self.pmc_measurements.get(divisor_pmc)
    # metrics = {
    #   AVG: dividend_measurement.avg()/divisor_measurement.avg(),
    #   MAX: dividend_measurement.max()/divisor_measurement.max(),
    #   MIN: dividend_measurement.min()/divisor_measurement.min(),
    #   STDDEV: dividend_measurement.stddev()/divisor_measurement.stddev(),
    #   TOT: dividend_measurement.total()/divisor_measurement.total(),
    #   COUNT: dividend_measurement.length()/divisor_measurement.length()
    # }
    # self.set_pmc_metrics(name,metrics)


  def calc_custom_pmcs_metric(self, name, pmc_names:List[str], calculator: Callable[[Dict[str,Measurement]],Dict[str,int or float]]):

    pmcs = {pmc: self.pmc_measurements.get(pmc,None) for pmc in pmc_names}
    metrics = calculator(pmcs)
    self.set_pmc_metrics(name,metrics)
  
  def has_any_metric(self) -> bool:
    return self.wcet_value or self.avg_time_value or self.has_pmc_metrics() or self.has_custom_metrics()

def _non_zero_div(dividend,divisor):
    return dividend/divisor if divisor != 0 else (dividend+1.0e-5)/1.0e-5