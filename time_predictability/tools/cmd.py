#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from getopt import getopt
import json
import sys
from typing import List, Dict
from time_predictability.mapping_exploration.explorer import ExplorationConfig, MappingExploration
from time_predictability.meta_parallel.app import App
from time_predictability.meta_parallel.task import Task

from time_predictability.meta_parallel.tdg import TDG
from time_predictability.meta_parallel.utils import EventChainMode, setAmxmiInfo
from time_predictability.simulation.scheduler import MasterQueue, task2Thread_algorithms, allocation_queue_algorithms
from time_predictability.simulation.simulator import Simulation, Simulator
from time_predictability.time_analysis.analysis import TDGAnalysis
from glob import glob

def dots2json():
    args = sys.argv[1:]    
    if len(args) < 1:
        print("usage: <config.cfg>")
        return
    config_path = args[0]
    with open(config_path,"r") as config_file:
        config:Dict = json.load(config_file)
        tdgs_regex = config["tdgs_dir"] +r'/*.dot'
        tdgs:List[str] = glob(tdgs_regex)

        app = App.read_dots(tdgs,config["app"])

        with open(config['output'],"w") as out:
            json.dump(app.to_json(), out,indent=2)

def amxmi2json():
    args = sys.argv[1:]    
    if len(args) < 1:
        print("usage: <config.cfg>")
        return
    config_path = args[0]
    with open(config_path,"r") as config_file:
        config:Dict = json.load(config_file)
        mode = None
        modeName = None
        if "latencyChain" in config:
            mode = EventChainMode.LATENCY_CONSTRAINT
            modeName = config["latencyChain"]
        elif "eventChain" in config:
            mode = EventChainMode.EVENT_CHAIN
            modeName = config["eventChain"]
        output = config["output"] if "output" in config else config["json"]
        setAmxmiInfo(config["model"],config["json"],mode,modeName,output)
   
def time_analysis():
    args = sys.argv[1:]    
    
    opts, args = getopt(args,"t:x:s:a:r:lch")
    if len(args) < 2:
        for o, v in opts:
            if o == "-l":
                print(time_analysis_metrics)
                return
        print(time_analysis_help_message)
        return
    input_tdg = args[0]
    output_tdg = args[1]
   
    analysis = TDGAnalysis()
    for o, v in opts:
        if o == "-t":
            analysis.num_threads = int(v)
        elif o == "-x":
            no_metrics = v.split(",")
            analysis.exclude(no_metrics)
        elif o in "-s":
            metrics = v.split(",")
            analysis.exclude_all()
            analysis.include(metrics) 
        elif o in "-a":
            metrics = v.split(",")
            analysis.include(metrics)
        elif o == "-r":
            params = v.split(",")
            if len(params) != 3:
                raise Exception("Ratio is composed by three parameters separated by comma: name,dividend,divisor. Given: "+v)
            n,d,r = params
            analysis.add_ratio(n,d,r)
        elif o in "-h":
            print(time_analysis_help_message)
            return
        elif o in "-l":
            print(time_analysis_metrics)
            return
        elif o in "-c":
            analysis.clear_results()
        else:
            assert False, "unhandled option "+o
    
    out_json = {}
    app: App = App.read_json(input_tdg,False)
    analysis.analyse_app(app)
    out_json = analysis.app_to_json(app)
    
    with open(output_tdg, 'w') as outfile:
        json.dump(out_json, outfile,indent=2)

def map_simulator():
    args = sys.argv[1:]    
    
    opts, args = getopt(args,"n:t:q:i:mchl")
    if len(args) < 2:
        for o, v in opts:
            if o == "-l":
                print(simulator_heuristics)
                return
        print(simulator_help_message)
        return

    num_threads = 1
    task2thread = "BFS"
    queue = "FIFO"
    queue_per_thread = False
    clear_results = False
    include_list = [] #i.e., all
    for o, v in opts:
        if o == "-n":
            num_threads = int(v)
        elif o == "-t":
            task2thread = v
        elif o == "-i":
            include_list = v.split(",")
        elif o in "-q":
            queue = v
        elif o in "-m":
            queue_per_thread = True
        elif o in "-h":
            print(simulator_help_message)
            return
        elif o in "-l":
            print(simulator_heuristics)
            return
        elif o in "-c":
            clear_results = True
        
        else:
            assert False, "unhandled option "+o
    
    input_tdg = args[0]
    output_tdg = args[1]

    app:App = App.read_json(input_tdg)

    tdg:TDG
    tdgs_list = app.tdgs if not include_list else (tdg for tdg in app.tdgs if tdg.id in include_list)
    for tdg in tdgs_list:
        task:Task
        for task in tdg.tasks.values():
            task.metrics.wcet() #guarantee that the tasks have a wcet associated
        
        taskQueue = MasterQueue.get_algorithm_by_name(task2thread,queue,queue_per_thread,num_threads)
        simulator:Simulator = Simulator(tdg,taskQueue(),num_threads)
        simulation:Simulation = simulator.simulate()
        tdg.set_static_threads(simulation.get_mapping(),simulation.makespan)

    out_json = app.to_json(keep_results=(not clear_results))
    with open(output_tdg, 'w') as outfile:
        json.dump(out_json, outfile,indent=2)

def map_exploration():
    args = sys.argv[1:]    
    opts, args = getopt(args,"chl")
    if len(args) < 1:
        print(exploration_help_message)
        return
    config_file = args[0]
    clear_results = False
    for o, v in opts:
        if o in "-h":
            print(exploration_help_message)
            return
        elif o in "-l":
            print(simulator_heuristics)
            return
        elif o in "-c":
            clear_results = True
        else:
            assert False, "unhandled option " + o
    
    config = None
    with open(config_file, 'r') as cfgfile:
        config = json.load(cfgfile)
    
    app: App = App.read_json(config['input'])
    if 'target_tdgs' in config:
        target_tdgs = config['target_tdgs']
    else:
        target_tdgs = [{"id": t.id, "deadline": -1} for t in app.tdgs]
    for target_tdg in target_tdgs:
        tdg = app.get_tdg(target_tdg["id"])
        deadline = target_tdg["deadline"]
        exp_config:ExplorationConfig = ExplorationConfig(config['num_threads'],deadline)
        for alg in config['map_algorithms']:
            exp_config.add_algorithm_from_dict(alg)
        
        # tdg:TDG = TDG.read_json(config['input'])
        explorer:MappingExploration = MappingExploration(tdg,exp_config)
        success = explorer.explore()
        if success:
            alg_info = explorer.config.algorithms_info[explorer.best_position]
            print('The best mapping found for tdg',tdg.id,'is '+alg_info['task2thread']+'+'+alg_info['queue'] + ' with a makespan of '+str(explorer.best_makespan))
            explorer.apply_map()
        else:
            print('[Warning] The exploration was not able to find a static mapping for tdg',tdg.id,'providing a makespan lower than the given deadline.')
            print('[Warning] The generated file will not contain a static mapping for tdg',tdg.id)
    out_json = app.to_json(keep_results=(not clear_results))
    output_tdg = config['output']
    with open(output_tdg, 'w') as outfile:
        json.dump(out_json, outfile,indent=2)


time_analysis_help_message = """usage: [options] <input tdg> <output tdg>
     ---------------------------------------------------------------------------
    | option | argument           | description                                 |
     --------|--------------------|---------------------------------------------
    | -h     | n/a                | show this message                           |
    | -t     | <num_threads>      | specify max number of threads               |
    | -l     | n/a                | list metrics                                |
    | -x     | <metric(,metric)*> | set metrics that should not be calculated   |
    | -s     | <metric(,metric)*> | set metrics that should be calculated       |
    | -a     | <metric(,metric)*> | append metrics that should be calculated    |
    | -c     | n/a                | remove 'results' property from tasks        |
     ---------------------------------------------------------------------------"""


simulator_help_message = """usage: [options] <input tdg> <output tdg>
     ----------------------------------------------------------------------------------
    | option | argument           | description                                        |
     --------|--------------------|----------------------------------------------------
    | -h     | n/a                | show this message                                  |
    | -i     | <ids>(,<ids>)*`    | list of target tdgs to apply mapping (def: all)    |
    | -l     | n/a                | list heuristics                                    |
    | -n     | <num_threads>      | specify number of threads available (def: 1)       |
    | -t     | <task2thread>      | specifies the scheduling algorithm (def: BFS)      |
    | -q     | <allocation_queue> | list metrics that should be calculated (def: FIFO) |
    | -m     | n/a                | use one queue per thread (instead of single queue) |
    | -c     | n/a                | remove 'results' property from tasks               |
     ----------------------------------------------------------------------------------"""


exploration_help_message = """usage: [options] <config.json>
     -=================================== Options ===========================-
    | option | argument  | description                                        |
     --------|-----------|----------------------------------------------------
    | -h     | n/a       | show this message                                  |
    | -l     | n/a       | list heuristics                                    |
    | -c     | n/a       | remove 'results' property from tasks               |
     -=======================================================================-

     -====================== Configuration File Properties ===================================-
    | property            | argument      | description                                        |
     ---------------------|---------------|----------------------------------------------------
    | input               | <tdg.json>    | input tdg json file                                |
    | output              | <tdg.json>    | output tdg json file                               |
    | num_threads         | int           | number of threads for the simulator                |
    | target_tdgs         | [target_tdg+] | list of target tdgs and corresponding deadline     |
    |   *target_tdg       | {id:int,deadline:int }| id of target tdg and its deadline         |
    | map_algorithms      | [algorithms+] | list of mapping algorithm specifications           |
     -========================================================================================-
     -=========================== For each algorithm =========================================-
    | property            | argument      | description                                        |
     ---------------------|---------------|----------------------------------------------------
    | task2thread         | <heuristic>   | one of the available heuristics for task2thread    |
    | queue               | <heuristic>   | one of the heuristics for allocation queue         |
    | queue_per_thread    | true|false    | specifies if the algorithm has one queue per thread|
     -========================================================================================-"""


simulator_heuristics = """-========== List of heuristics ===========-
task2thread (-t option): {task2Thread_algorithms}
allocation queue (-q option): {allocation_queue_algorithms}
-=========================================-""".format(task2Thread_algorithms=", ".join(task2Thread_algorithms.keys()),
    allocation_queue_algorithms=", ".join(allocation_queue_algorithms.keys()))


time_analysis_metrics = """
 -=========================== Metrics ============================-
| key         | name                              | level | active |
|-------------|-----------------------------------|-------| ------ |
| wcet        | worst case execution time         | task  |    x   |
| pmc         | performance counter metrics       | task  |    x   |
| volume      | Volume                            | TDG   |    x   |
| cpl         | critical path length              | TDG   |    x   |
| max_par     | maximum parallelism               | TDG   |    x   |
| depth       | depth                             | TDG   |    x   |
| makespan    | makespan (requires static_thread) | TDG   |        |
| wcrt        | worst case response time          | TDG   |    x   |
 -================================================================-"""